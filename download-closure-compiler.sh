#!/bin/bash -e
#						 ↑
# debug [-x -v]:[complete, abbreviated]
# Brief:	brief
# Author: Johnny Cubides
# e-mail: jgcubidesc@gmail.com 
# date: Wednesday 23 February 2022
status=$?

#VCC VERSION CLOSURE COMPILER
VCC=v20220202

wget -O "closure-compiler.jar" https://repo1.maven.org/maven2/com/google/javascript/closure-compiler/${VCC}/closure-compiler-${VCC}.jar
