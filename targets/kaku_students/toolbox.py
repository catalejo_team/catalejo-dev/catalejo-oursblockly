#!/bin/python

import sys

nameToolboxVarJS = "toolbox"
if sys.argv[2] != None:
    nameToolboxVarJS = sys.argv[2]

def main():
    if sys.argv[1] == 'export':
        export()
    else:
        default()


def default():
    with open("toolbox.xml", "r") as fp:
        file = open("toolbox.js", "w")
        file.write('let ' + nameToolboxVarJS + ' = "";\n')
        for line in fp:
            line = line.replace('\'', '\\\'')
            file.write(nameToolboxVarJS + ' += '+'\''+line.replace('\n', "")+'\';\n')
        file.close()

def export():
    with open("toolbox.xml", "r") as fp:
        file = open("toolbox.js", "w")
        file.write('export const ' + nameToolboxVarJS + ' = \n`\n')
        for line in fp:
            file.write(line)
        file.write('`')
        file.close()
    file = open("toolbox.d.ts", "w")
    file.write('export const ' + nameToolboxVarJS + ': string')
    file.close()


main()
