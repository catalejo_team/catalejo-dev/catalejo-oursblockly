#!/bin/bash -e
#
# Brief:	Empaquetador de archivos.
# Author: Johnny Cubides
# e-mail: jgcubidesc@gmail.com, johnnycubides@catalejoplus.com
# date: Sunday 03 February 2019

# foreground
BLACK=`tput setaf 0`
RED=`tput setaf 1`
GREEN=`tput setaf 2`
YELLOW=`tput setaf 3`
BLUE=`tput setaf 4`
MAGENTA=`tput setaf 5`
CYAN=`tput setaf 6`
WHITE=`tput setaf 7`
NC=`tput setaf 7`
# background
BLACKB=`tput setab 0`
REDB=`tput setab 1`
GREENB=`tput setab 2`
YELLOWB=`tput setab 3`
BLUEB=`tput setab 4`
MAGENTAB=`tput setab 5`
CYANB=`tput setab 6`
WHITEB=`tput setab 7`
NCB=`tput setab 0`

TARGET=${2:-usermodules}.js

DIST=${2:-dist}
DIST=dist/$DIST

function mobile() {
  # FIND='FieldImage("dependencies/modulesNodeMCU/javascript/imag/'
  # REPLACE='FieldImage("./modes/default/blockly/imag/'
  # sed -i \'s/$FIND/$REPLACE/g\' $DIST/$TARGET
  # sed -i 's/"dependencies\/modulesNodeMCU\/javascript\/imag\//"\.\/modes\/luabot\/blockly\/imag\//g' $DIST/$TARGET
  # Remplazar por requerimiento específico
  FIND=dependencies/modulesNodeMCU/javascript/imag/
  REPLACE=./img/imag/
  sed -i "s!$FIND!$REPLACE!g" $DIST/$TARGET
  # Se requiere agregar esta linea para poder importar correctamente
  sed -i "1i import Blockly from \'blockly\';" $DIST/$TARGET
}

function general() {
  mkdir -p $DIST

  rm -rf $DIST/*
  java -jar ../../closure-compiler.jar --js *.js --js_output_file $DIST/$TARGET

  cp -r img $DIST

  if [ "$1" = "mobile" ];
  then
    python toolbox.py "export"
  else
    python toolbox.py "no export"
  fi

  mv toolbox.js toolbox.d.ts $DIST
}

function desktop() {
  # FIND='FieldImage("dependencies/modulesNodeMCU/javascript/imag/'
  # REPLACE='FieldImage("./modes/default/blockly/imag/'
  # sed -i \'s/$FIND/$REPLACE/g\' $DIST/$TARGET
  # sed -i 's/"dependencies\/modulesNodeMCU\/javascript\/imag\//"\.\/modes\/luabot\/blockly\/imag\//g' $DIST/$TARGET
  FIND=dependencies/modulesNodeMCU/javascript/imag/
  REPLACE=./modes/default/blockly/imag/
  sed -i "s!$FIND!$REPLACE!g" $DIST/$TARGET
}

# Permite guardar información acerca de comandos usados
if [ "$1" = "-h" ] || [ "$1" = "" ] || [ "$1" = "--help" ];
then
    printf "Help for this command make_pack.sh\n"
    printf "\t${CYAN}make_pack.sh${NC} Command options\n"
    printf "\t[Commands]\n"
    printf "\t\t${CYAN}mobile${NC} ${YELLOW}name_dist${NC}\tpack for mobile app\n"
    printf "\t\t${CYAN}desktop${NC} ${YELLOW}name_dist${NC}\tpack for catalejo-editor\n"
    printf "\t\t-h,--help\tHelp\n"
    printf "\n${GREEN}Regards Johnny.${NC}\n"
elif [ "$1" = "mobile" ];
then
  general "$1"
  mobile
elif [ "$1" = "desktop" ];
then
  general "$1"
  desktop
fi
