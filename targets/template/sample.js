// Inserta aquí las salidas de los bloques generados con blockly factory
// blockly factory: https://blockly-demo.appspot.com/static/demos/blockfactory/index.html
// blockly factory old:

// sample.js representa en este caso la categoría MIS BLOQUES
// en otros casos puede representar la categoría: sensores, actuadores, etc.

//  -- PRIMER BLOQUE DE ESTA CATEGORÍA ---
// PARTE VISUAL DEL BLOQUE
Blockly.Blocks['mi_bloque'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("mi primer bloque");
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(230);
 this.setTooltip("");
 this.setHelpUrl("");
  }
};

// GENERADOR DE CÓDIGO PARA OBJETIVO, ejemplo micropython
Blockly.Python['mi_bloque'] = function(block) {
  // TODO: Assemble Python into code variable.
  // var code = '...\n'; // esta es la parte del código que debe ser modificada según objetivo
  // ejemplo:
  var code = 'print("Hola mundo")\n';
  return code;
};

//  -- SEGUNDO BLOQUE DE ESTA CATEGORÍA ---
//  ...
