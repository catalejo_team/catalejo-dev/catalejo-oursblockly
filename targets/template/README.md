# Compilación de modulesNodeMCU

El script **make_pack.sh** es responsable de compilar las librerías creadas para luabotv2;
éstas librerías son las quedarán en el toolbox de la aplicación para que el usuario
pueda desarrollar sus proyectos.

```bash
./make_pack.sh mobile template # para versión movil
```

```bash
./make_pack.sh desktop template # para versión desktop
```

Johnny Cubides
