Blockly.Blocks['math_int_random'] = {
  init: function() {
    this.appendValueInput("number")
        .setCheck("int")
        .appendField("Entero aleatorio desde 0 a");
    this.setOutput(true, "int");
    this.setColour(230);
 this.setTooltip("Genera un entero aleatorio entre 0 a el número especificado");
 this.setHelpUrl("");
  }
};

Blockly.Blocks['math_int_random2'] = {
  init: function() {
    this.appendValueInput("min")
        .setCheck("int")
        .appendField("Entero aleatorio desde");
    this.appendValueInput("max")
        .setCheck("int")
        .appendField("a");
    this.setInputsInline(true);
    this.setOutput(true, "int");
    this.setColour(230);
 this.setTooltip("Genera un entero aleatorio entre dos números, ejemplo: 2 a 10, 0, 127.");
 this.setHelpUrl("");
  }
};

Blockly.Blocks['math_float_random'] = {
  init: function() {
    this.appendDummyInput()
        .appendField(" aleatorio desde 0 a 1");
    this.setOutput(true, "float");
    this.setColour(230);
 this.setTooltip("Genera un número aleatorio entre 0 a 1, ejemplos de resultados: 0.1, 0.5");
 this.setHelpUrl("");
  }
};

Blockly.Blocks['math_float_random2'] = {
  init: function() {
    this.appendValueInput("min")
        .setCheck(["int", "float"])
        .appendField("Aleatorio desde");
    this.appendValueInput("max")
        .setCheck(["int", "float"])
        .appendField("a");
    this.setInputsInline(true);
    this.setOutput(true, "float");
    this.setColour(230);
 this.setTooltip("Genera un número aleatorio entre un Max y Min (el rango deberá estar entre 0 y 1) , ejemplos 0.05 - 1, 0 - 0.1.");
 this.setHelpUrl("");
  }
};
