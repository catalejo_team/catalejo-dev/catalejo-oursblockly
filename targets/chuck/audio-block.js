Blockly.Blocks['rec_chuck'] = {
  init: function() {
    this.appendDummyInput()
        .setAlign(Blockly.ALIGN_CENTRE)
        .appendField("Grabar audio")
        .appendField(new Blockly.FieldImage("./chuckImg/audio/mic.png", 30, 30, { alt: "*", flipRtl: "FALSE" }));
    this.appendValueInput("filename")
        .setCheck("string")
        .appendField("Nombre de archivo");
    this.appendValueInput("volumen")
        .setCheck(["float", "int"])
        .appendField("Volumen de grabación");
    this.setColour(60);
 this.setTooltip("Este bloque se debe poner en un buffer en solitario, es decir, sin otros bloques. Configurar el nombre del archivo usando un nombre sin tildes ni caracteres especiales como la ñ, con respecto al volumen puede usar un valor entre 0 a 1.");
 this.setHelpUrl("");
  }
};

Blockly.Blocks['dac_chuck'] = {
  init: function() {
    this.appendValueInput("oscilator")
        .setCheck(["SinOsc", "TriOsc", "PulseOsc", "SawOsc", "SqrOsc", "Rhodey", "Clarinet", "SndBuf"])
        .appendField(new Blockly.FieldImage("./chuckImg/audio/speaker.png", 20, 20, { alt: "*", flipRtl: "FALSE" }))
        .appendField("Conectar a audio:");
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(60);
 this.setTooltip("Conectar un oscilador o un instrumento al audio del PC.");
 this.setHelpUrl("");
  }
};

Blockly.Blocks['osc_cfg_chuck'] = {
  init: function() {
    this.appendValueInput("var_osc")
        .setCheck(["SinOsc", "TriOsc", "PulseOsc"])
        .appendField("Oscilador:");
    this.appendValueInput("value")
        .setCheck(["int", "float"])
        .appendField(new Blockly.FieldDropdown([["Frecuencia","freq"], ["Ganancia","gain"]]), "cfg_member")
        .appendField(":");
    this.setInputsInline(true);
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(0);
 this.setTooltip("Valor: número, Oscilador: variable de un oscilador");
 this.setHelpUrl("");
  }
};

Blockly.Blocks['osc_play'] = {
  init: function() {
    this.appendValueInput("osc_var")
        .setCheck(null)
        .appendField("Osc:");
    this.appendValueInput("note")
        .setCheck(["int"])
        .appendField("play:");
    this.setInputsInline(true);
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(0);
 this.setTooltip("Ejecute una nota midi, osc representa un oscilador, en play poner una nota o numero entero");
 this.setHelpUrl("");
  }
};

// -- Oscilador play tiempo
Blockly.Blocks['osc_play_tempo'] = {
  init: function() {
    this.appendValueInput("osc_var")
        .setCheck(null)
        .appendField("Osc:");
    this.appendValueInput("note")
        .setCheck("int")
        .appendField("Play:");
    this.appendValueInput("tempo")
        .setCheck("dur")
        .appendField("Tiempo:");
    this.setInputsInline(true);
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(0);
 this.setTooltip("Ejecute una nota midi, osc representa un oscilador, en play poner una nota o numero entero, en tiempo poner duración");
 this.setHelpUrl("");
  }
};
