Blockly.Blocks['midi2freq_std_chuck'] = {
  init: function() {
    this.appendValueInput("midi")
        .setCheck(["int", "float"])
        .appendField("frecuencia <= midi");
    this.setOutput(true, ["int", "float"]);
    this.setColour(260);
 this.setTooltip("Convierte una nota midi en frecuencia");
 this.setHelpUrl("");
  }
};

Blockly.Chuck['midi2freq_std_chuck'] = function(block) {
  var value_midi = Blockly.Chuck.valueToCode(block, 'midi', Blockly.Chuck.ORDER_ATOMIC);
  // TODO: Assemble Chuck into code variable.
  var code = value_midi + ' => Std.mtof ';
  // TODO: Change ORDER_NONE to the correct strength.
  return [code, Blockly.Chuck.ORDER_NONE];
};
