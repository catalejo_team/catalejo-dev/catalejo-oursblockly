Blockly.Chuck['create_array_chuck'] = function(block) {
  var dropdown_type = block.getFieldValue('type');
  var value_var = Blockly.Chuck.valueToCode(block, 'var', Blockly.Chuck.ORDER_ATOMIC);
  var value_size = Blockly.Chuck.valueToCode(block, 'size', Blockly.Chuck.ORDER_ATOMIC);
  // TODO: Assemble Chuck into code variable.
  var code = dropdown_type + ' ' + value_var + '[' + value_size + '];\n';
  return code;
};

Blockly.Chuck['set_val_pos_array_chuck'] = function(block) {
  var value_variable = Blockly.Chuck.valueToCode(block, 'variable', Blockly.Chuck.ORDER_ATOMIC);
  var value_position = Blockly.Chuck.valueToCode(block, 'position', Blockly.Chuck.ORDER_ATOMIC);
  var value_value = Blockly.Chuck.valueToCode(block, 'value', Blockly.Chuck.ORDER_ATOMIC);
  // TODO: Assemble Chuck into code variable.
  var code = value_value + ' => ' + value_variable + '[' + value_position +'];\n';
  return code;
};

Blockly.Chuck['get_val_pos_array_chuck'] = function(block) {
  var value_variable = Blockly.Chuck.valueToCode(block, 'variable', Blockly.Chuck.ORDER_ATOMIC);
  var value_position = Blockly.Chuck.valueToCode(block, 'position', Blockly.Chuck.ORDER_ATOMIC);
  // TODO: Assemble Chuck into code variable.
  var code = value_variable + '[' + value_position + ']';
  // TODO: Change ORDER_NONE to the correct strength.
  return [code, Blockly.Chuck.ORDER_NONE];
};

Blockly.Chuck['array_length'] = function(block) {
  var value_array = Blockly.Chuck.valueToCode(block, 'array', Blockly.Chuck.ORDER_ATOMIC);
  // TODO: Assemble Chuck into code variable.
  var code = value_array + ".cap()";
  // TODO: Change ORDER_NONE to the correct strength.
  return [code, Blockly.Chuck.ORDER_NONE];
};

Blockly.Chuck['array_declare'] = function(block) {
  var value_name_list = Blockly.Chuck.valueToCode(block, 'name_list', Blockly.Chuck.ORDER_ATOMIC);
  var dropdown_type = block.getFieldValue('type');
  var value_array = Blockly.Chuck.valueToCode(block, 'array', Blockly.Chuck.ORDER_ATOMIC);
  // TODO: Assemble Chuck into code variable.
  var code = value_array + " @=> " + dropdown_type + " " + value_name_list + "[];\n";
  return code;
};
