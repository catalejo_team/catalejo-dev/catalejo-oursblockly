Blockly.Blocks['for_chuck'] = {
  init: function() {
    this.appendValueInput("var")
        .setCheck(["int", "float"])
        .appendField("Contar con variable");
    this.appendValueInput("init")
        .setCheck(["int", "float"])
        .appendField("desde");
    this.appendValueInput("until")
        .setCheck(["int", "float"])
        .appendField("hasta");
    this.appendValueInput("step")
        .setCheck(["int", "float"])
        .appendField("de a");
    this.appendStatementInput("instructions")
        .setCheck(null)
        .appendField("Hacer");
    this.setInputsInline(true);
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(120);
 this.setTooltip("Crear un bucle contando con una variable tipo entera, flotante o duración, desde [número], hasta [número], de a [número]");
 this.setHelpUrl("");
  }
};

Blockly.Chuck['for_chuck'] = function(block) {
  var value_var = Blockly.Chuck.valueToCode(block, 'var', Blockly.Chuck.ORDER_ATOMIC);
  var value_init = Blockly.Chuck.valueToCode(block, 'init', Blockly.Chuck.ORDER_ATOMIC);
  var value_until = Blockly.Chuck.valueToCode(block, 'until', Blockly.Chuck.ORDER_ATOMIC);
  var value_step = Blockly.Chuck.valueToCode(block, 'step', Blockly.Chuck.ORDER_ATOMIC);
  var statements_instructions = Blockly.Chuck.statementToCode(block, 'instructions');
  // TODO: Assemble Chuck into code variable.

  // value_step = value_step.replace("(", "");
  // value_step = value_step.replace(")", "");

  // var condiction;
  // var increments;
  // if ( value_init < value_until ) {
  //   condiction = value_var + " <= " + value_until ;
  //   if (Math.abs(value_step) == 1) {
  //     increments = value_var + "++"
  //   } else if (Math.abs(value_step) > 1) {
  //     increments = Math.abs(value_step) + " +=> " + value_var;
  //   }
  // } else if ( value_init > value_until ) {
  //   condiction = value_var + " >= " + value_until ;
  //   if (Math.abs(value_step) == 1) {
  //     increments = value_var + "--"
  //   } else if (Math.abs(value_step) > 1) {
  //     increments = Math.abs(value_step) + " -=> " + value_var;
  //   }
  // } else {
  //   condiction = value_var + " <= " + value_until ;
  //   increments = value_var + "++"
  // }
  // var code = "for ("+ value_init + " => " + value_var +"; " + condiction +"; " + increments + ") { \n" +
  //   statements_instructions +
  // "}\n";

  function getFeatures(value) {
    value = value.replace("(", "");
    value = value.replace(")", "");
    const isNumber = !isNaN(value);
    const number = Number(value);
    const abs = Math.abs(number)
    return { value: value, isNumber: isNumber, number: number, abs: abs };
  }

  var condiction;
  var increments;
  const variable = getFeatures(value_var);
  const init = getFeatures(value_init);
  const until = getFeatures(value_until);
  const step = getFeatures(value_step);

  if (init.isNumber && until.isNumber && step.isNumber) {
    if ( init.number < until.number ) {
      condiction = value_var + " <= " + value_until ;
      if (step.abs == 1) {
        increments = value_var + "++"
      } else if (step.abs > 1) {
        increments = step.abs + " +=> " + value_var;
      }
    } else if ( init.number > until.number ) {
      condiction = value_var + " >= " + value_until ;
      if (step.abs == 1) {
        increments = value_var + "--"
      } else if (step.abs > 1) {
        increments = Math.abs(value_step) + " -=> " + value_var;
      }
    } else {
      condiction = value_var + " <= " + value_until ;
      increments = value_var + "++"
    }
  } else {

  }

  return code;
};
