Blockly.Chuck['file_sound'] = function(block) {
  var value_sndbuf_var = Blockly.Chuck.valueToCode(block, 'sndbuf_var', Blockly.Chuck.ORDER_ATOMIC);
  var value_path_file = Blockly.Chuck.valueToCode(block, 'path_file', Blockly.Chuck.ORDER_ATOMIC);
  // TODO: Assemble Chuck into code variable.
  var code = "me.dir() + " + value_path_file + " => " + value_sndbuf_var + ".read;\n";
  return code;
};

Blockly.Chuck['file_sound_gain'] = function(block) {
  var value_sndbuf_var = Blockly.Chuck.valueToCode(block, 'sndbuf_var', Blockly.Chuck.ORDER_ATOMIC);
  var value_gain = Blockly.Chuck.valueToCode(block, 'gain', Blockly.Chuck.ORDER_ATOMIC);
  // TODO: Assemble Chuck into code variable.
  var code = value_gain + " => " + value_sndbuf_var + ".gain;\n";
  return code;
};

Blockly.Chuck['file_sound_pos_play'] = function(block) {
  var value_sndbuf_var = Blockly.Chuck.valueToCode(block, 'sndbuf_var', Blockly.Chuck.ORDER_ATOMIC);
  var value_pos = Blockly.Chuck.valueToCode(block, 'pos', Blockly.Chuck.ORDER_ATOMIC);
  // TODO: Assemble Chuck into code variable.
  var code = value_pos + " => " + value_sndbuf_var + ".pos;\n";
  return code;
};
