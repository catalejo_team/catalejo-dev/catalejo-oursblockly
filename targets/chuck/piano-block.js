Blockly.Blocks['piano_nota_dur_on_off'] = {
  init: function() {
    this.appendValueInput("var")
        .setCheck("Rhodey")
        .appendField(new Blockly.FieldImage("./chuckImg/instruments/piano.png", 20, 20, { alt: "Piano", flipRtl: "FALSE" }))
        .appendField("Piano:");
    this.appendValueInput("note")
        .setCheck(["int", "float"])
        .appendField("Nota:");
    this.appendValueInput("dur")
        .setCheck(["int", "float", "dur"])
        .appendField("Dur:");
    this.appendValueInput("note_on")
        .setCheck(["int", "float", "dur"])
        .appendField("On:");
    this.appendValueInput("note_off")
        .setCheck(["int", "float"])
        .appendField("Off:");
    this.setInputsInline(true);
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(230);
 this.setTooltip("Piano: variable tipo piano, Nota: nota midi, Dur: Tiempo, On: Velocidad de encendido, Off: velocidad de apagado");
 this.setHelpUrl("");
  }
};

Blockly.Blocks['piano_noteon'] = {
  init: function() {
    this.appendValueInput("instrument")
        .setCheck("Rhodey")
        .appendField(new Blockly.FieldImage("./chuckImg/instruments/piano.png", 20, 20, { alt: "*", flipRtl: "FALSE" }))
        .appendField("Piano:");
    this.appendValueInput("velocity")
        .setCheck(["int", "float"])
        .appendField("ON:");
    this.setInputsInline(true);
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(230);
 this.setTooltip("Activar nota, Piano: variable tipo piano, ON: velocidad de encendido generalmente entre 0 - 1");
 this.setHelpUrl("");
  }
};

Blockly.Blocks['piano_noteoff'] = {
  init: function() {
    this.appendValueInput("instrument")
        .setCheck("Rhodey")
        .appendField(new Blockly.FieldImage("./chuckImg/instruments/piano.png", 20, 20, { alt: "*", flipRtl: "FALSE" }))
        .appendField("Piano:");
    this.appendValueInput("velocity")
        .setCheck(["int", "float"])
        .appendField("OFF:");
    this.setInputsInline(true);
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(230);
 this.setTooltip("Desativar nota nota, Piano: variable tipo piano, ON: velocidad de apagado generalmente entre 0 - 1");
 this.setHelpUrl("");
  }
};

Blockly.Blocks['piano_gain'] = {
  init: function() {
    this.appendValueInput("instrument")
        .setCheck("Rhodey")
        .appendField(new Blockly.FieldImage("./chuckImg/instruments/piano.png", 20, 20, { alt: "*", flipRtl: "FALSE" }))
        .appendField("Piano:");
    this.appendValueInput("gain")
        .setCheck(["int", "float"])
        .appendField("Volumen");
    this.setInputsInline(true);
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(230);
 this.setTooltip("Control de volumen. Piano: variable tipo piano, Volumen: volumen del piano, valor entre 0 - 1, ejemplo: 0.5");
 this.setHelpUrl("");
  }
};

Blockly.Blocks['piano_note'] = {
  init: function() {
    this.appendValueInput("instrument")
        .setCheck("Rhodey")
        .appendField(new Blockly.FieldImage("./chuckImg/instruments/piano.png", 20, 20, { alt: "*", flipRtl: "FALSE" }))
        .appendField("Piano:");
    this.appendValueInput("note")
        .setCheck(["int", "float"])
        .appendField("Nota:");
    this.setInputsInline(true);
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(230);
 this.setTooltip("Configurar nota midi, Piano: variable tipo piano, Nota: Nota midi, ejemplo: 60 que representa el Do 4 octava");
 this.setHelpUrl("");
  }
};

Blockly.Blocks['piano_freq'] = {
  init: function() {
    this.appendValueInput("instrument")
        .setCheck("Rhodey")
        .appendField(new Blockly.FieldImage("./chuckImg/instruments/piano.png", 20, 20, { alt: "*", flipRtl: "FALSE" }))
        .appendField("Piano:");
    this.appendValueInput("note")
        .setCheck(["int", "float"])
        .appendField("Frecuencia");
    this.setInputsInline(true);
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(230);
 this.setTooltip("Configurar frecuencia, Piano: variable tipo piano, Frecuencia:  Ejemplo:  263 que representa el Do 4 octava");
 this.setHelpUrl("");
  }
};