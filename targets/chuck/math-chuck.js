Blockly.Chuck['math_int_random'] = function(block) {
  var value_number = Blockly.Chuck.valueToCode(block, 'number', Blockly.Chuck.ORDER_ATOMIC);
  // TODO: Assemble Chuck into code variable.
  var code = "Math.random(" + value_number + ")";
  // TODO: Change ORDER_NONE to the correct strength.
  return [code, Blockly.Chuck.ORDER_NONE];
};

Blockly.Chuck['math_int_random2'] = function(block) {
  var value_min = Blockly.Chuck.valueToCode(block, 'min', Blockly.Chuck.ORDER_ATOMIC);
  var value_max = Blockly.Chuck.valueToCode(block, 'max', Blockly.Chuck.ORDER_ATOMIC);
  // TODO: Assemble Chuck into code variable.
  var code = "Math.random2(" + value_min + ", " + value_max + ")";
  // TODO: Change ORDER_NONE to the correct strength.
  return [code, Blockly.Chuck.ORDER_NONE];
};

Blockly.Chuck['math_float_random'] = function(block) {
  // TODO: Assemble Chuck into code variable.
  var code = "Math.randomf()";
  // TODO: Change ORDER_NONE to the correct strength.
  return [code, Blockly.Chuck.ORDER_NONE];
};

Blockly.Chuck['math_float_random2'] = function(block) {
  var value_min = Blockly.Chuck.valueToCode(block, 'min', Blockly.Chuck.ORDER_ATOMIC);
  var value_max = Blockly.Chuck.valueToCode(block, 'max', Blockly.Chuck.ORDER_ATOMIC);
  // TODO: Assemble Chuck into code variable.
  var code = "Math.randomf2(" + value_min + ", " + value_max + ")";
  // TODO: Change ORDER_NONE to the correct strength.
  return [code, Blockly.Chuck.ORDER_NONE];
};
