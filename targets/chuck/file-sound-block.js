Blockly.Blocks['file_sound'] = {
  init: function() {
    this.appendValueInput("sndbuf_var")
        .setCheck("SndBuf")
        .appendField("Pista de audio");
    this.appendValueInput("path_file")
        .setCheck("string")
        .appendField("asignar archivo");
    this.setInputsInline(true);
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(30);
 this.setTooltip("A una pista de audio representada por una variable asignar una ruta de un archivo de audio que está en el PC. sonido_de_audio.wav");
 this.setHelpUrl("");
  }
};

Blockly.Blocks['file_sound_gain'] = {
  init: function() {
    this.appendValueInput("sndbuf_var")
        .setCheck("SndBuf")
        .appendField("Pista de audio");
    this.appendValueInput("gain")
        .setCheck(["int", "float"])
        .appendField("volumen");
    this.setInputsInline(true);
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(30);
 this.setTooltip("A una pista de audio representada por una variable tipo archivo asignar un volumen entre 0 a 1, ejemplo, 0.1, 0.5.");
 this.setHelpUrl("");
  }
};

Blockly.Blocks['file_sound_pos_play'] = {
  init: function() {
    this.appendValueInput("sndbuf_var")
        .setCheck("SndBuf")
        .appendField("Pista de audio");
    this.appendValueInput("pos")
        .setCheck(["int", "float"])
        .appendField("Iniciar desde");
    this.setInputsInline(true);
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(30);
 this.setTooltip("A una pista de audio representada por una variable tipo archivo iniciar desde la posición de una muestra (sample), ejemplo: 0");
 this.setHelpUrl("");
  }
};

