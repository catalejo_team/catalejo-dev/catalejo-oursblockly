Blockly.Chuck['rec_chuck'] = function(block) {
  var value_filename = Blockly.Chuck.valueToCode(block, 'filename', Blockly.Chuck.ORDER_ATOMIC);
  var value_volumen = Blockly.Chuck.valueToCode(block, 'volumen', Blockly.Chuck.ORDER_ATOMIC);
  // TODO: Assemble Chuck into code variable.
  //
  // chuck this with other shreds to record to file
  // example> chuck foo.ck bar.ck rec (see also rec2.ck)

  // pull samples from the dac
  var code = "dac => Gain g => WvOut w => blackhole;\n" +
  // set the prefix, which will prepended to the filename
  // do this if you want the file to appear automatically
  // in another directory.  if this isn't set, the file
  // should appear in the directory you run chuck from
  // with only the date and time.
  "me.dir() + " + value_filename + " => w.autoPrefix;\n" +
  // this is the output file name
  "\"special:auto\" => w.wavFilename;\n" +
  // print it out
  "<<<\"writing to file: \", w.filename()>>>;\n" +
  // any gain you want for the output
  value_volumen + " => g.gain;\n" +
  // temporary workaround to automatically close file on remove-shred
  "null @=> w;\n" +
  // infinite time loop...
  // ctrl-c will stop it, or modify to desired duration
  "while( true ) 1::second => now;\n";
  return code;
};

Blockly.Chuck['dac_chuck'] = function(block) {
  var value_oscilator = Blockly.Chuck.valueToCode(block, 'oscilator', Blockly.Chuck.ORDER_ATOMIC);
  // TODO: Assemble Chuck into code variable.
  var code = value_oscilator + ' => dac;\n';
  return code;
};

Blockly.Chuck['osc_cfg_chuck'] = function(block) {
  var value_var_osc = Blockly.Chuck.valueToCode(block, 'var_osc', Blockly.Chuck.ORDER_ATOMIC);
  var dropdown_cfg_member = block.getFieldValue('cfg_member');
  var value_config = Blockly.Chuck.valueToCode(block, 'value', Blockly.Chuck.ORDER_ATOMIC);
  // TODO: Assemble Chuck into code variable.
  var code = value_config + " => " + value_var_osc + '.' + dropdown_cfg_member + ';\n';
  return code;
};

Blockly.Chuck['osc_play'] = function(block) {
  var value_osc_var = Blockly.Chuck.valueToCode(block, 'osc_var', Blockly.Chuck.ORDER_ATOMIC);
  var value_note = Blockly.Chuck.valueToCode(block, 'note', Blockly.Chuck.ORDER_ATOMIC);
  // TODO: Assemble Chuck into code variable.
  var code = value_note + " => Std.mtof => " + value_osc_var + ".freq;\n";
  return code;
};

Blockly.Chuck['osc_play_tempo'] = function(block) {
  var value_osc_var = Blockly.Chuck.valueToCode(block, 'osc_var', Blockly.Chuck.ORDER_ATOMIC);
  var value_note = Blockly.Chuck.valueToCode(block, 'note', Blockly.Chuck.ORDER_ATOMIC);
  var value_tempo = Blockly.Chuck.valueToCode(block, 'tempo', Blockly.Chuck.ORDER_ATOMIC);
  // TODO: Assemble Chuck into code variable.
  var code = value_note + " => Std.mtof => " + value_osc_var + ".freq, " + value_tempo + " => now;\n";
  return code;
};
