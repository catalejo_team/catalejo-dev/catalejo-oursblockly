Blockly.Chuck['piano_nota_dur_on_off'] = function(block) {
  var value_var = Blockly.Chuck.valueToCode(block, 'var', Blockly.Chuck.ORDER_ATOMIC);
  var value_note = Blockly.Chuck.valueToCode(block, 'note', Blockly.Chuck.ORDER_ATOMIC);
  var value_dur = Blockly.Chuck.valueToCode(block, 'dur', Blockly.Chuck.ORDER_ATOMIC);
  var value_note_on = Blockly.Chuck.valueToCode(block, 'note_on', Blockly.Chuck.ORDER_ATOMIC);
  var value_note_off = Blockly.Chuck.valueToCode(block, 'note_off', Blockly.Chuck.ORDER_ATOMIC);
  // TODO: Assemble Chuck into code variable.
  var code = value_note + " => Std.mtof => " + value_var + ".freq;\n" +
    value_note_on + " => " + value_var + ".noteOn;\n" +
    value_dur + " => now;\n" +
    value_note_off + " => " + value_var + ".noteOff;\n";
  return code;
};

Blockly.Chuck['piano_noteon'] = function(block) {
  var value_instrument = Blockly.Chuck.valueToCode(block, 'instrument', Blockly.Chuck.ORDER_ATOMIC);
  var value_velocity = Blockly.Chuck.valueToCode(block, 'velocity', Blockly.Chuck.ORDER_ATOMIC);
  // TODO: Assemble Chuck into code variable.
  var code = value_velocity + " => " + value_instrument + ".noteOn;\n" ;
  return code;
};

Blockly.Chuck['piano_noteoff'] = function(block) {
  var value_instrument = Blockly.Chuck.valueToCode(block, 'instrument', Blockly.Chuck.ORDER_ATOMIC);
  var value_velocity = Blockly.Chuck.valueToCode(block, 'velocity', Blockly.Chuck.ORDER_ATOMIC);
  // TODO: Assemble Chuck into code variable.
  var code = value_velocity + " => " + value_instrument + ".noteOff;\n" ;
  return code;
};

Blockly.Chuck['piano_gain'] = function(block) {
  var value_instrument = Blockly.Chuck.valueToCode(block, 'instrument', Blockly.Chuck.ORDER_ATOMIC);
  var value_gain = Blockly.Chuck.valueToCode(block, 'gain', Blockly.Chuck.ORDER_ATOMIC);
  // TODO: Assemble Chuck into code variable.
  var code = value_gain + " => " + value_instrument + ".gain;\n";
  return code;
};

Blockly.Chuck['piano_note'] = function(block) {
  var value_instrument = Blockly.Chuck.valueToCode(block, 'instrument', Blockly.Chuck.ORDER_ATOMIC);
  var value_note = Blockly.Chuck.valueToCode(block, 'note', Blockly.Chuck.ORDER_ATOMIC);
  // TODO: Assemble Chuck into code variable.
  var code = value_note + " => Std.mtof => " + value_instrument + ".freq;\n";
  return code;
};

Blockly.Chuck['piano_freq'] = function(block) {
  var value_instrument = Blockly.Chuck.valueToCode(block, 'instrument', Blockly.Chuck.ORDER_ATOMIC);
  var value_note = Blockly.Chuck.valueToCode(block, 'note', Blockly.Chuck.ORDER_ATOMIC);
  // TODO: Assemble Chuck into code variable.
  var code = value_note + " => " + value_instrument + ".freq;\n";
  return code;
};
