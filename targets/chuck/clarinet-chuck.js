Blockly.Chuck['clarinet_nota_dur_on_off'] = function(block) {
  var value_instrument = Blockly.Chuck.valueToCode(block, 'var', Blockly.Chuck.ORDER_ATOMIC);
  var value_note = Blockly.Chuck.valueToCode(block, 'note', Blockly.Chuck.ORDER_ATOMIC);
  var value_dur = Blockly.Chuck.valueToCode(block, 'dur', Blockly.Chuck.ORDER_ATOMIC);
  var value_note_on = Blockly.Chuck.valueToCode(block, 'note_on', Blockly.Chuck.ORDER_ATOMIC);
  var value_note_off = Blockly.Chuck.valueToCode(block, 'note_off', Blockly.Chuck.ORDER_ATOMIC);
  // TODO: Assemble Chuck into code variable.
  var code = value_note + " => Std.mtof => " + value_instrument + ".freq;\n" +
    value_note_on + " => " + value_instrument + ".noteOn;\n" +
    value_dur + " => now;\n" +
    value_note_off + " => " + value_instrument + ".noteOff;\n";
  return code;
};

Blockly.Chuck['clarinet_noteon'] = function(block) {
  var value_instrument = Blockly.Chuck.valueToCode(block, 'instrument', Blockly.Chuck.ORDER_ATOMIC);
  var value_velocity = Blockly.Chuck.valueToCode(block, 'velocity', Blockly.Chuck.ORDER_ATOMIC);
  // TODO: Assemble Chuck into code variable.
  var code = value_velocity + " => " + value_instrument + ".noteOn;\n";
  return code;
};

Blockly.Chuck['clarinet_noteoff'] = function(block) {
  var value_instrument = Blockly.Chuck.valueToCode(block, 'instrument', Blockly.Chuck.ORDER_ATOMIC);
  var value_velocity = Blockly.Chuck.valueToCode(block, 'velocity', Blockly.Chuck.ORDER_ATOMIC);
  // TODO: Assemble Chuck into code variable.
  var code = value_velocity + " => " + value_instrument + ".noteOff;\n";
  return code;
};

Blockly.Chuck['clarinet_vibrato_freq'] = function(block) {
  var value_instrument = Blockly.Chuck.valueToCode(block, 'instrument', Blockly.Chuck.ORDER_ATOMIC);
  var value_freq = Blockly.Chuck.valueToCode(block, 'freq', Blockly.Chuck.ORDER_ATOMIC);
  // TODO: Assemble Chuck into code variable.
  var code = value_freq + " => " + value_instrument + ".vibratoFreq;\n";
  return code;
};

Blockly.Chuck['clarinet_vibrato_gain'] = function(block) {
  var value_instrument = Blockly.Chuck.valueToCode(block, 'instrument', Blockly.Chuck.ORDER_ATOMIC);
  var value_gain = Blockly.Chuck.valueToCode(block, 'gain', Blockly.Chuck.ORDER_ATOMIC);
  // TODO: Assemble Chuck into code variable.
  var code = value_gain + " => " + value_instrument + ".vibratoGain;\n";
  return code;
};

Blockly.Chuck['clarinet_pressure'] = function(block) {
  var value_instrument = Blockly.Chuck.valueToCode(block, 'instrument', Blockly.Chuck.ORDER_ATOMIC);
  var value_pressure = Blockly.Chuck.valueToCode(block, 'pressure', Blockly.Chuck.ORDER_ATOMIC);
  // TODO: Assemble Chuck into code variable.
  var code = value_pressure + " => " + value_instrument + ".pressure;\n";
  return code;
};

Blockly.Chuck['clarinet_note'] = function(block) {
  var value_instrument = Blockly.Chuck.valueToCode(block, 'instrument', Blockly.Chuck.ORDER_ATOMIC);
  var value_note = Blockly.Chuck.valueToCode(block, 'note', Blockly.Chuck.ORDER_ATOMIC);
  // TODO: Assemble Chuck into code variable.
  var code = value_note + " => Std.mtof => " + value_instrument + ".freq;\n";
  return code;
};
