Blockly.Blocks['now_chuck'] = {
  init: function() {
    this.appendValueInput("time")
        .setCheck(["dur", "int", "float"])
        .appendField("Duración:");
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(65);
 this.setTooltip("Se selecciona cuanto tiempo durará una acción");
 this.setHelpUrl("");
  }
};

Blockly.Blocks['dur_select_chuck'] = {
  init: function() {
    this.appendValueInput("number")
        .setCheck(["int", "float"]);
    this.appendDummyInput()
        .appendField(new Blockly.FieldDropdown([["segundos","second"], ["minutos","minute"], ["milisegundos","ms"], ["muestra","samp"], ["horas","hour"], ["días","day"], ["semenas","week"]]), "dur");
    this.setInputsInline(true);
    this.setOutput(true, "dur");
    this.setColour(65);
 this.setTooltip("");
 this.setHelpUrl("");
  }
};

// -- BPM
Blockly.Blocks['bpm'] = {
  init: function() {
    this.appendValueInput("bpm")
        .setCheck("int")
        .appendField(new Blockly.FieldImage("./chuckImg/time/metronome.png", 20, 20, { alt: "*", flipRtl: "FALSE" }))
        .appendField("Definir PPM");
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(65);
 this.setTooltip("Número de pulsaciones por minuto, se recomienda poner al principio del código");
 this.setHelpUrl("");
  }
};

//-- Figuras musicales
Blockly.Blocks['figure_tempo'] = {
  init: function() {
    this.appendDummyInput()
        .appendField(new Blockly.FieldDropdown([[{"src":"./chuckImg/figure/redonda.png","width":15,"height":15,"alt":"Redonda"},"Redonda"], [{"src":"./chuckImg/figure/blanca.png","width":15,"height":15,"alt":"Blanca"},"Blanca"], [{"src":"./chuckImg/figure/negra.png","width":15,"height":15,"alt":"Negra"},"Negra"], [{"src":"./chuckImg/figure/corchea.png","width":15,"height":15,"alt":"Corchea"},"Corchea"], [{"src":"./chuckImg/figure/semicorchea.png","width":15,"height":15,"alt":"Semicorchea"},"Semicorchea"], [{"src":"./chuckImg/figure/fusa.png","width":15,"height":15,"alt":"Fusa"},"Fusa"], [{"src":"./chuckImg/figure/semifusa.png","width":15,"height":15,"alt":"Semifusa"},"Semifusa"]]), "figure");
    this.setOutput(true, "dur");
    this.setColour(65);
 this.setTooltip("Selecciona una figura musical");
 this.setHelpUrl("");
  }
};

Blockly.Blocks['figure_silence'] = {
  init: function() {
    this.appendDummyInput()
        .appendField(new Blockly.FieldDropdown([[{"src":"./chuckImg/figure/redonda_silencio.png","width":15,"height":15,"alt":"*"},"redonda"], [{"src":"./chuckImg/figure/blanca_silencio.png","width":15,"height":15,"alt":"*"},"blanca"], [{"src":"./chuckImg/figure/negra_silencio.png","width":15,"height":15,"alt":"*"},"negra"], [{"src":"./chuckImg/figure/corchea_silencio.png","width":15,"height":15,"alt":"*"},"corchea"], [{"src":"./chuckImg/figure/semicorchea_silencio.png","width":15,"height":15,"alt":"*"},"semicorchea"], [{"src":"./chuckImg/figure/fusa_silencio.png","width":15,"height":15,"alt":"*"},"fusa"], [{"src":"./chuckImg/figure/semifusa_silencio.png","width":15,"height":15,"alt":"*"},"semifusa"]]), "figure");
    this.setOutput(true, null);
    this.setColour(65);
 this.setTooltip("Seleccionar una figura que representa un silencio en la música");
 this.setHelpUrl("");
  }
};
