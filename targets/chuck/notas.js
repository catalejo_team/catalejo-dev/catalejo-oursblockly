Blockly.Blocks['notas2midi_chuck'] = {
  init: function() {
    this.appendDummyInput()
        .appendField(new Blockly.FieldDropdown([["Do","0"], ["Do#","1"], ["Re","2"], ["Re#","3"], ["Mi","4"], ["Fa","5"], ["Fa#","6"], ["Sol","7"], ["Sol#","8"], ["La","9"], ["La#","10"], ["Si","11"]]), "note")
        .appendField(new Blockly.FieldDropdown([["-1","0"], ["0","1"], ["1","2"], ["2","3"], ["3","4"], ["4","5"], ["5","6"], ["6","7"], ["7","8"], ["8","9"], ["9","10"]]), "eighth");
    this.setOutput(true, ["int", "float"]);
    this.setColour(260);
    this.setTooltip("Nota musical a midi [Nota] [Octava], ej. Do4 = 60 midi");
    this.setHelpUrl("");
  }
};

Blockly.Chuck['notas2midi_chuck'] = function(block) {
  var dropdown_note = block.getFieldValue('note');
  var dropdown_eighth = block.getFieldValue('eighth');
  // TODO: Assemble Chuck into code variable.
  var code = (+dropdown_eighth)*12 + (+dropdown_note);
  // TODO: Change ORDER_NONE to the correct strength.
  return [code, Blockly.Chuck.ORDER_NONE];
};

Blockly.Blocks['nota2freq_chuck'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("frecuencia <=")
        .appendField(new Blockly.FieldDropdown([["Do","0"], ["Do#","1"], ["Re","2"], ["Re#","3"], ["Mi","4"], ["Fa","5"], ["Fa#","6"], ["Sol","7"], ["Sol#","8"], ["La","9"], ["La#","10"], ["Si","11"]]), "note")
        .appendField(new Blockly.FieldDropdown([["-1","0"], ["0","1"], ["1","2"], ["2","3"], ["3","4"], ["4","5"], ["5","6"], ["6","7"], ["7","8"], ["8","9"], ["9","10"]]), "eighth");
    this.setOutput(true, ["int", "float"]);
    this.setColour(260);
 this.setTooltip("Nota musical a frecuencia [Nota] [Octava], ej. Do4 = 263 Hz");
 this.setHelpUrl("");
  }
};

Blockly.Chuck['nota2freq_chuck'] = function(block) {
  var dropdown_note = block.getFieldValue('note');
  var dropdown_eighth = block.getFieldValue('eighth');
  // TODO: Assemble Chuck into code variable.
  var code = (+dropdown_eighth)*12 + (+dropdown_note) + ' => Std.mtof ';
  // TODO: Change ORDER_NONE to the correct strength.
  return [code, Blockly.Chuck.ORDER_NONE];
};

