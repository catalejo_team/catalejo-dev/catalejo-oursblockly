Blockly.Chuck['spork'] = function(block) {
  var statements_functions = Blockly.Chuck.statementToCode(block, 'functions');
  // TODO: Assemble Chuck into code variable.

  const split = statements_functions.split("\n")

  var code = "";

  for (let i = 0, len = split.length; i < len; i++) {
    if (split[i] != "") {
      code += "spork ~" + split[i] + "\n";
    }
  }
  if (code != "") {
    code += "while (true ) 1::second => now;\n";
  }

  return code;
};
