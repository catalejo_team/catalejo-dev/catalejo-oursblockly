Blockly.Chuck['now_chuck'] = function(block) {
  var value_time = Blockly.Chuck.valueToCode(block, 'time', Blockly.Chuck.ORDER_ATOMIC);
  // TODO: Assemble Chuck into code variable.
  var code = value_time + ' => now;\n';
  return code;
};

Blockly.Chuck['dur_select_chuck'] = function(block) {
  var value_number = Blockly.Chuck.valueToCode(block, 'number', Blockly.Chuck.ORDER_ATOMIC);
  var dropdown_dur = block.getFieldValue('dur');
  // TODO: Assemble Chuck into code variable.
  var code = value_number + ' :: ' + dropdown_dur;
  // TODO: Change ORDER_NONE to the correct strength.
  return [code, Blockly.Chuck.ORDER_NONE];
};

Blockly.Chuck['bpm'] = function(block) {
  var value_bpm = Blockly.Chuck.valueToCode(block, 'bpm', Blockly.Chuck.ORDER_ATOMIC);
  // TODO: Assemble Chuck into code variable.
    //Asignación de la duración del compas, por ejemplo, 1 segundo
  var bpm = 60 / value_bpm;
  var code = bpm + " :: second => dur compas;\n" +
    "compas/2 => dur Negra; //Cuarta\n" +
    "4*Negra => dur Redonda; //Entera\n" +
    "2*Negra => dur Blanca; //Media\n" +
    "Negra/2 => dur Corchea; //Octava\n" +
    "Negra/4 => dur Semicorchea; //Dieciseisava\n" +
    "Negra/8 => dur Fusa; //Treintaidosava\n" +
    "Negra/16 => dur Semifusa; //Sesentacuatroava\n";
  return code;
};

Blockly.Chuck['figure_tempo'] = function(block) {
  var dropdown_figure = block.getFieldValue('figure');
  // TODO: Assemble Chuck into code variable.
  var code = dropdown_figure;
  // TODO: Change ORDER_NONE to the correct strength.
  return [code, Blockly.Chuck.ORDER_NONE];
};

Blockly.Chuck['figure_silence'] = function(block) {
  var dropdown_figure = block.getFieldValue('figure');
  // TODO: Assemble Chuck into code variable.
  var code = dropdown_figure;
  // TODO: Change ORDER_NONE to the correct strength.
  return [code, Blockly.Chuck.ORDER_NONE];
};
