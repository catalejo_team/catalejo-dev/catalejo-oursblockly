Blockly.Blocks['osc_out_config'] = {
  init: function() {
    this.appendValueInput("var_osc_out")
        .setCheck("OscOut")
        .appendField("Configurar OSC de salida. OSC enviar");
    this.appendValueInput("ip")
        .setCheck("string")
        .appendField("IP");
    this.appendValueInput("port")
        .setCheck("int")
        .appendField("Puerto");
    this.setInputsInline(true);
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(160);
 this.setTooltip("Configuración del OSC (open sound control) para enviar datos a otros dispositivos, en \"OSC enviar\" poner una variable del tipo \"OSC enviar\", en IP y Puerto poner una IP y puerto válido de un dispositivo que esté en red al que se le va a enviar el mensaje por OSC.");
 this.setHelpUrl("");
  }
};

Blockly.Chuck['osc_out_config'] = function(block) {
  var value_var_osc_out = Blockly.Chuck.valueToCode(block, 'var_osc_out', Blockly.Chuck.ORDER_ATOMIC);
  var value_ip = Blockly.Chuck.valueToCode(block, 'ip', Blockly.Chuck.ORDER_ATOMIC);
  var value_port = Blockly.Chuck.valueToCode(block, 'port', Blockly.Chuck.ORDER_ATOMIC);
  // TODO: Assemble Chuck into code variable.
  var code = value_var_osc_out + ".dest(" + value_ip + ", " + value_port + ");\n";
  return code;
};

Blockly.Blocks['osc_send'] = {
  init: function() {
    this.appendDummyInput()
        .setAlign(Blockly.ALIGN_CENTRE)
        .appendField("Enviar por OSC");
    this.appendValueInput("osc_out")
        .setCheck("OscOut")
        .setAlign(Blockly.ALIGN_RIGHT)
        .appendField("OSC enviar");
    this.appendValueInput("address")
        .setCheck("string")
        .setAlign(Blockly.ALIGN_RIGHT)
        .appendField("Dirección");
    this.appendStatementInput("osc_out_format")
        .setCheck(null)
        .appendField("Datos");
    this.setInputsInline(false);
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(160);
 this.setTooltip("Envío de datos por OSC, en \"Variable\" ponga la variable del tipo \"OSC enviar\" previamente creada y configurada, en \"Dirección\" ponga el texto que representa a qué está dirigido los datos, ejemplo \"/piano\", en \"Datos\" agregue los datos a enviar en el formato específico.");
 this.setHelpUrl("");
  }
};

Blockly.Chuck['osc_send'] = function(block) {
  var value_osc_out = Blockly.Chuck.valueToCode(block, 'osc_out', Blockly.Chuck.ORDER_ATOMIC);
  var value_address = Blockly.Chuck.valueToCode(block, 'address', Blockly.Chuck.ORDER_ATOMIC);
  var statements_osc_out_format = Blockly.Chuck.statementToCode(block, 'osc_out_format');
  // TODO: Assemble Chuck into code variable.
  var code = value_osc_out + ".start(" + value_address + ")" +
    statements_osc_out_format +
    ".send();\n";
  return code;
};

Blockly.Blocks['osc_out_iifs_fotmat'] = {
  init: function() {
    this.appendDummyInput()
        .setAlign(Blockly.ALIGN_CENTRE)
        .appendField("Formato OSC");
    this.appendValueInput("int1")
        .setCheck("int")
        .appendField("Pos 0, 1er entero");
    this.appendValueInput("int2")
        .setCheck("int")
        .appendField("Pos 1, 2do entero");
    this.appendValueInput("float")
        .setCheck("float")
        .appendField("Pos 2, Decimal");
    this.appendValueInput("text")
        .setCheck("string")
        .appendField("Pos 3, Texto");
    this.setPreviousStatement(true, null);
    this.setColour(160);
 this.setTooltip("Los datos serán formateados para que puedan ser enviados en el protocolo OSC. Como se especifica en cada entrada del bloque solo podrá enviar dos enteros, un número decimal y un texto, puede hacer uso algunos o todos los campos, debe tener en cuenta el orden de los datos en el dispositivo que los va a recibir, la palabra \"Pos\" hace referencia a la posición requerida para tal fin.");
 this.setHelpUrl("");
  }
};

Blockly.Chuck['osc_out_iifs_fotmat'] = function(block) {
  var value_int1 = Blockly.Chuck.valueToCode(block, 'int1', Blockly.Chuck.ORDER_ATOMIC);
  var value_int2 = Blockly.Chuck.valueToCode(block, 'int2', Blockly.Chuck.ORDER_ATOMIC);
  var value_float = Blockly.Chuck.valueToCode(block, 'float', Blockly.Chuck.ORDER_ATOMIC);
  var value_text = Blockly.Chuck.valueToCode(block, 'text', Blockly.Chuck.ORDER_ATOMIC);
  // TODO: Assemble Chuck into code variable.
  var code = ".add(" + value_int1 + ")" +
    ".add(" + value_int2 + ")" +
    ".add(" + value_float + ")" +
    ".add(" + value_text + ")";

  return code;
};

Blockly.Blocks['osc_out_format'] = {
  init: function() {
    this.appendValueInput("data")
        .setCheck(["int", "float", "string"])
        .appendField("Agregar dato");
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(160);
 this.setTooltip("Permite agregar diferentes datos que serán enviados por \"OSC\" representados por la variable del tipo \"OSC enviar\", como entrada recibe datos del tipo entero, decimal y texto");
 this.setHelpUrl("");
  }
};

Blockly.Chuck['osc_out_format'] = function(block) {
  var value_data = Blockly.Chuck.valueToCode(block, 'data', Blockly.Chuck.ORDER_ATOMIC);
  // TODO: Assemble Chuck into code variable.
  var code = ".add(" + value_data + ")";
  return code;
};

Blockly.Blocks['osc_in_config'] = {
  init: function() {
    this.appendValueInput("var_osc_in")
        .setCheck("OscIn")
        .appendField("Configurar OSC de entrada. OSC recibir");
    this.appendValueInput("port")
        .setCheck("int")
        .appendField("Puerto");
    this.setInputsInline(true);
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(160);
 this.setTooltip("Configuración del OSC (open sound control) para recibir datos provenientes de otros dispositivos, en \"OSC recibir\" poner una variable del tipo \"OSC recibir\", en Puerto poner un puerto válido de escucha para que otros dispositivos puedan enviar datos a él por OSC.");
 this.setHelpUrl("");
  }
};

Blockly.Chuck['osc_in_config'] = function(block) {
  var value_var_osc_in = Blockly.Chuck.valueToCode(block, 'var_osc_in', Blockly.Chuck.ORDER_ATOMIC);
  var value_port = Blockly.Chuck.valueToCode(block, 'port', Blockly.Chuck.ORDER_ATOMIC);
  // TODO: Assemble Chuck into code variable.
  var code = value_port + " => " + value_var_osc_in + ".port;\n" + value_var_osc_in + ".listenAll();\n"
  return code;
};

Blockly.Blocks['osc_received'] = {
  init: function() {
    this.appendDummyInput()
        .setAlign(Blockly.ALIGN_CENTRE)
        .appendField("Recibir por OSC");
    this.appendValueInput("osc_in")
        .setCheck("OscIn")
        .setAlign(Blockly.ALIGN_RIGHT)
        .appendField("OSC recibir");
    this.appendValueInput("osc_msg")
        .setCheck("OscMsg")
        .setAlign(Blockly.ALIGN_RIGHT)
        .appendField("OSC mensaje");
    this.appendDummyInput()
        .setAlign(Blockly.ALIGN_CENTRE)
        .appendField("Si recibe datos");
    this.appendStatementInput("osc_data_in")
        .setCheck(null)
        .appendField("hacer");
    this.setInputsInline(false);
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(160);
 this.setTooltip("Recepción de datos por protocolo OSC, en \"variable\" deberá agregar la variable de tipo \"OSC recibir\" que haya sido configurada, en \"mensaje\" debe agregar la variable de tipo \"OSC mensaje\" y en hacer, puede hacer uso de la bloque que obtiene de la variable mensaje los datos de el tipo deseado (entero, decimal o texto) según la posición del dato.");
 this.setHelpUrl("");
  }
};

Blockly.Chuck['osc_received'] = function(block) {
  var value_osc_in = Blockly.Chuck.valueToCode(block, 'osc_in', Blockly.Chuck.ORDER_ATOMIC);
  var value_osc_msg = Blockly.Chuck.valueToCode(block, 'osc_msg', Blockly.Chuck.ORDER_ATOMIC);
  var statements_osc_data_in = Blockly.Chuck.statementToCode(block, 'osc_data_in');
  // TODO: Assemble Chuck into code variable.
  var code = value_osc_in + " => now;\n" +
    "while(" + value_osc_in + ".recv(" + value_osc_msg + "))\n" +
    "{\n" +
    statements_osc_data_in +
    "}\n";

  return code;
};

Blockly.Blocks['osc_in_msg'] = {
  init: function() {
    this.appendValueInput("data")
        .setCheck("OscMsg")
        .appendField("OSC mensaje");
    this.appendValueInput("position")
        .setCheck("int")
        .appendField("posición");
    this.appendDummyInput()
        .appendField("obtener")
        .appendField(new Blockly.FieldDropdown([["Entero","getInt"], ["Decimal","getFloat"], ["Texto","getString"]]), "method");
    this.setInputsInline(true);
    this.setOutput(true, null);
    this.setColour(160);
 this.setTooltip("Obtener un dato de los mensajes recibidos por OSC. En \"Desde\" agregar la variable del tipo \"OSC mensaje\" que contiene los mensajes recibidos desde otros dispositivos en RED, en \"posición\" se debe poner la posición del mensaje que se quiere obtener y finalmente en \"obtener\" se debe indicar el tipo de dato obtenido.");
 this.setHelpUrl("");
  }
};

Blockly.Chuck['osc_in_msg'] = function(block) {
  var value_data = Blockly.Chuck.valueToCode(block, 'data', Blockly.Chuck.ORDER_ATOMIC);
  var value_position = Blockly.Chuck.valueToCode(block, 'position', Blockly.Chuck.ORDER_ATOMIC);
  var dropdown_method = block.getFieldValue('method');
  // TODO: Assemble Chuck into code variable.
  var code = value_data + "." + dropdown_method + "(" + value_position + ")";
  // TODO: Change ORDER_NONE to the correct strength.
  return [code, Blockly.Chuck.ORDER_NONE];
};

Blockly.Blocks['osc_in_msg_address'] = {
  init: function() {
    this.appendValueInput("data")
        .setCheck("OscMsg")
        .appendField("Obtener dirección de OSC mensaje");
    this.setInputsInline(true);
    this.setOutput(true, null);
    this.setColour(160);
 this.setTooltip("Obtener la dirección de una variable del tipo \"OSC mensaje\", deberá poner una variable del tipo indicado en el espacio vacío. El valor obtenido puede ser comparado con un texto para determinar el destinatario del mensaje y actuar según corresponda, por ejemplo haciendo uso de un bloque condicional puede discriminar qué proceso ejecutar");
 this.setHelpUrl("");
  }
};

Blockly.Chuck['osc_in_msg_address'] = function(block) {
  var value_data = Blockly.Chuck.valueToCode(block, 'data', Blockly.Chuck.ORDER_ATOMIC);
  // TODO: Assemble Chuck into code variable.
  var code = value_data + ".address";
  // TODO: Change ORDER_NONE to the correct strength.
  return [code, Blockly.Chuck.ORDER_NONE];
};
