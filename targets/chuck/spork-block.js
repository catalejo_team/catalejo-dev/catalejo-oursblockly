Blockly.Blocks['spork'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("Multitarea");
    this.appendStatementInput("functions")
        .setCheck(null);
    this.setPreviousStatement(true, null);
    this.setColour(290);
 this.setTooltip("Introducir aquí las funciones que se requieran ejecutar en multitarea o multihilos de ejecución");
 this.setHelpUrl("");
  }
};
