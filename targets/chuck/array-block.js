Blockly.Blocks['create_array_chuck'] = {
  init: function() {
    this.appendValueInput("var")
        .setCheck("array")
        .appendField("Crear lista de datos, tipo:")
        .appendField(new Blockly.FieldDropdown([["Entero","int"], ["Flotante","float"], ["Duración","dur"], ["Oscilador seno","SinOsc"], ["Oscilador triangular","TriOsc"], ["Oscilador pulso","PulseOsc"], ["Oscilador Cuadrada","SqrOsc"], ["Oscilador sierra","SawOsc"]]), "type")
        .appendField("variable:");
    this.appendValueInput("size")
        .setCheck("int")
        .appendField("tamaño:");
    this.setInputsInline(true);
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(260);
    this.setTooltip("Crea una lista de datos de un tamaño definido");
    this.setHelpUrl("");
  }
};

Blockly.Blocks['set_val_pos_array_chuck'] = {
  init: function() {
    this.appendValueInput("variable")
        .setCheck("array")
        .appendField("Llenar lista: ");
    this.appendValueInput("position")
        .setCheck("int")
        .appendField("posición:");
    this.appendValueInput("value")
        .setCheck(null)
        .appendField("valor:");
    this.setInputsInline(true);
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(260);
    this.setTooltip("Llenar lista colocando un valor en una posición específica de la lista.");
    this.setHelpUrl("");
  }
};

Blockly.Blocks['get_val_pos_array_chuck'] = {
  init: function() {
    this.appendValueInput("variable")
        .setCheck("array")
        .appendField("obtener valor, lista:");
    this.appendValueInput("position")
        .setCheck("int")
        .appendField("posición");
    this.setInputsInline(true);
    this.setOutput(true, null);
    this.setColour(260);
 this.setTooltip("Obtener el valor de una posición de la lista");
 this.setHelpUrl("");
  }
};

Blockly.Blocks['array_length'] = {
  init: function() {
    this.appendValueInput("array")
        .setCheck("array")
        .appendField("Tamaño de");
    this.setOutput(true, "int");
    this.setColour(260);
 this.setTooltip("Obtener el tamaño de una lista, ejemplo: [1, 5, 3] es una lista de tamaño 3.");
 this.setHelpUrl("");
  }
};

Blockly.Blocks['array_declare'] = {
  init: function() {
    this.appendValueInput("name_list")
        .setCheck("array")
        .appendField("Crear lista");
    this.appendDummyInput()
        .appendField("del tipo")
        .appendField(new Blockly.FieldDropdown([["Entero","int"], ["Flotante","float"], ["Duración","dur"]]), "type");
    this.appendValueInput("array")
        .setCheck(null)
        .appendField("Con los valores");
    this.setInputsInline(false);
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(260);
 this.setTooltip("Obtener el tamaño de una lista, ejemplo: [1, 5, 3] es una lista de tamaño 3.");
 this.setHelpUrl("");
  }
};
