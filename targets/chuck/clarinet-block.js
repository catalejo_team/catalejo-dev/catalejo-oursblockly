Blockly.Blocks['clarinet_nota_dur_on_off'] = {
  init: function() {
    this.appendValueInput("var")
        .setCheck("Clarinet")
        .appendField(new Blockly.FieldImage("./chuckImg/instruments/clarinet.png", 20, 20, { alt: "Clarinete", flipRtl: "FALSE" }))
        .appendField("Clarinete:");
    this.appendValueInput("note")
        .setCheck(["int", "float"])
        .appendField("Nota:");
    this.appendValueInput("dur")
        .setCheck(["int", "float", "dur"])
        .appendField("Dur:");
    this.appendValueInput("note_on")
        .setCheck(["int", "float", "dur"])
        .appendField("On:");
    this.appendValueInput("note_off")
        .setCheck(["int", "float"])
        .appendField("Off:");
    this.setInputsInline(true);
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(120);
 this.setTooltip("Clarinete: variable tipo clarinete, Nota: nota midi, Dur: Tiempo, On: Velocidad de encendido, Off: velocidad de apagado");
 this.setHelpUrl("");
  }
};

Blockly.Blocks['clarinet_noteon'] = {
  init: function() {
    this.appendValueInput("instrument")
        .setCheck("Clarinet")
        .appendField(new Blockly.FieldImage("./chuckImg/instruments/clarinet.png", 20, 20, { alt: "Clarinete", flipRtl: "FALSE" }))
        .appendField("Clarinete");
    this.appendValueInput("velocity")
        .setCheck(["int", "float"])
        .appendField("ON:");
    this.setInputsInline(true);
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(120);
 this.setTooltip("Activar nota, Clarinet: variable tipo clarinete, ON: velocidad de encendido generalmente entre 0 - 1");
 this.setHelpUrl("");
  }
};

Blockly.Blocks['clarinet_noteoff'] = {
  init: function() {
    this.appendValueInput("instrument")
        .setCheck("Clarinet")
        .appendField(new Blockly.FieldImage("./chuckImg/instruments/clarinet.png", 20, 20, { alt: "Clarinete", flipRtl: "FALSE" }))
        .appendField("Clarinete:");
    this.appendValueInput("velocity")
        .setCheck(["int", "float"])
        .appendField("OFF:");
    this.setInputsInline(true);
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(120);
 this.setTooltip("Desactivar nota nota, Clarinete: variable tipo clarinete, ON: velocidad de apagado generalmente entre 0 - 1");
 this.setHelpUrl("");
  }
};

Blockly.Blocks['clarinet_vibrato_freq'] = {
  init: function() {
    this.appendValueInput("instrument")
        .setCheck("Clarinet")
        .appendField(new Blockly.FieldImage("./chuckImg/instruments/clarinet.png", 20, 20, { alt: "Clarinete", flipRtl: "FALSE" }))
        .appendField("Clarinete");
    this.appendValueInput("freq")
        .setCheck(["int", "float"])
        .appendField("Frecuencia de vibración");
    this.setInputsInline(true);
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(120);
 this.setTooltip("Configurar frecuencia de vibración, Clarinet: variable tipo clarinete, Frecuencia de vibración: Ejemplos: 100, 330,  etc.");
 this.setHelpUrl("");
  }
};

Blockly.Blocks['clarinet_vibrato_gain'] = {
  init: function() {
    this.appendValueInput("instrument")
        .setCheck("Clarinet")
        .appendField(new Blockly.FieldImage("./chuckImg/instruments/clarinet.png", 20, 20, { alt: "Clarinete", flipRtl: "FALSE" }))
        .appendField("Clarinete");
    this.appendValueInput("gain")
        .setCheck(["int", "float"])
        .appendField("Ganancia de vibración");
    this.setInputsInline(true);
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(120);
 this.setTooltip("Configurar ganancia de vibración. Clarinet: variable tipo clarinete, Ganancia de vibración: valor entre [0-1], ejemplo: 0.5,  etc.");
 this.setHelpUrl("");
  }
};

Blockly.Blocks['clarinet_pressure'] = {
  init: function() {
    this.appendValueInput("instrument")
        .setCheck("Clarinet")
        .appendField(new Blockly.FieldImage("./chuckImg/instruments/clarinet.png", 20, 20, { alt: "Clarinete", flipRtl: "FALSE" }))
        .appendField("Clarinete");
    this.appendValueInput("pressure")
        .setCheck(["int", "float"])
        .appendField("Volumen");
    this.setInputsInline(true);
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(120);
 this.setTooltip("Configurar volumen del clarinete. Clarinet: variable tipo clarinete, Volumen: valor entre [0-1], ejemplo: 0.5,  etc.");
 this.setHelpUrl("");
  }
};

Blockly.Blocks['clarinet_note'] = {
  init: function() {
    this.appendValueInput("instrument")
        .setCheck("Clarinet")
        .appendField(new Blockly.FieldImage("./chuckImg/instruments/clarinet.png", 20, 20, { alt: "Clarinete", flipRtl: "FALSE" }))
        .appendField("Clarinete");
    this.appendValueInput("note")
        .setCheck(["int", "float"])
        .appendField("Nota");
    this.setInputsInline(true);
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(120);
 this.setTooltip("Configurar una nota en formato midi. Clarinet: variable tipo clarinete, Nota: Nota en formato midi, ejemplo: 60 (que representa el Do en 4 octava.");
 this.setHelpUrl("");
  }
};