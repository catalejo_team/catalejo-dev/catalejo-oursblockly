Blockly.Blocks['midi'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("MIDI");
    this.appendValueInput("status")
        .setCheck("Number")
        .appendField("Estado");
    this.appendValueInput("data1")
        .setCheck("Number")
        .appendField("Dato 1");
    this.appendValueInput("data2")
        .setCheck("Number")
        .appendField("Dato 2");
    this.setInputsInline(true);
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(265);
    this.setTooltip('');
    this.setHelpUrl('http://www.example.com/');
  }
};

Blockly.Lua['midi'] = function(block) {
  var value_status = Blockly.Lua.valueToCode(block, 'status', Blockly.Lua.ORDER_ATOMIC);
  var value_data1 = Blockly.Lua.valueToCode(block, 'data1', Blockly.Lua.ORDER_ATOMIC);
  var value_data2 = Blockly.Lua.valueToCode(block, 'data2', Blockly.Lua.ORDER_ATOMIC);
  // TODO: Assemble Lua into code variable.
  var code = 'uart.write(0, string.char('+value_status+'), string.char('+value_data1+'), string.char('+value_data2+'))\n';
  return code;
};

// INIT OSC SERVICE
Blockly.Blocks['service_osc_init'] = {
  init: function() {
    this.appendDummyInput()
        .setAlign(Blockly.ALIGN_CENTRE)
        .appendField("Iniciar servicio OSC");
    this.setInputsInline(false);
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(160);
 this.setTooltip("Hace inicio del servicio OSC (control de sonido abierto), este protocolo permite la comunicación de dispositivos en red para enviar y recibir datos.");
 this.setHelpUrl("");
  }
};
Blockly.Lua['service_osc_init'] = function(block) {
  // TODO: Assemble Lua into code variable.
  var code = "local sk_osc = net.createUDPSocket()\n";
  return code;
};

// SERVICIO OSC IN UDP
Blockly.Blocks['service_osc_listen'] = {
  init: function() {
    this.appendDummyInput()
        .setAlign(Blockly.ALIGN_CENTRE)
        .appendField("Recibir por OSC");
    this.appendValueInput("port")
        .setCheck("Number")
        .setAlign(Blockly.ALIGN_RIGHT)
        .appendField("Puerto de escucha");
    this.appendDummyInput()
        .appendField("Si recibe datos almacenar en OSC_IN y");
    this.appendStatementInput("received")
        .setCheck(null)
        .appendField("hacer");
    this.setInputsInline(false);
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(160);
 this.setTooltip("Recibir datos por servicio OSC y almacenar en la variable OSC_IN, se requiere configurar un puerto de escucha, para hacer uso de OSC_IN buscar el bloque correspondiente en el toolbox.");
 this.setHelpUrl("");
  }
};

Blockly.Lua['service_osc_listen'] = function(block) {
  var value_port = Blockly.Lua.valueToCode(block, 'port', Blockly.Lua.ORDER_ATOMIC);
  var statements_received = Blockly.Lua.statementToCode(block, 'received');
  // TODO: Assemble Lua into code variable.
  var code = "local osc_udp = {}\n" +
    "local function receive(_, data)\n" +
    "   osc_udp.a, osc_udp.i1, osc_udp.i2, osc_udp.f, osc_udp.s = osc.get_iifs(string.len(data), data)\n" +
        statements_received +
    "end\n" +
    "sk_osc:listen(" + value_port + ")\n" +
    "sk_osc:on(\"receve\", receive)\n";
  return code;
};

// DATA OSC_IN

Blockly.Blocks['osc_in_data'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("OSC_IN.")
        .appendField(new Blockly.FieldDropdown([["Dirección","a"], ["1er entero","i1"], ["2do entero","i2"], ["Decimal","f"], ["Texto","s"]]), "member");
    this.setOutput(true, null);
    this.setColour(160);
 this.setTooltip("OSC_IN representa los datos que son recibidos por el protocolo OSC; OSC_IN.Dirección indica a qué elemento está dirigida la información, OSC_IN.1er o 2do entero son datos de números enteros, OSC_IN.Decimal es un dato en formato decimal y OSC_IN.Texto representa un dato de tipo Texto.");
 this.setHelpUrl("");
  }
};

Blockly.Lua['osc_in_data'] = function(block) {
  var dropdown_member = block.getFieldValue('member');
  // TODO: Assemble Lua into code variable.
  var code = 'osc_udp.' + dropdown_member;
  // TODO: Change ORDER_NONE to the correct strength.
  return [code, Blockly.Lua.ORDER_NONE];
};

Blockly.Blocks['service_osc_send'] = {
  init: function() {
    this.appendDummyInput()
        .setAlign(Blockly.ALIGN_CENTRE)
        .appendField("Enviar por OSC");
    this.appendValueInput("ip")
        .setCheck("String")
        .setAlign(Blockly.ALIGN_RIGHT)
        .appendField("IP");
    this.appendValueInput("port")
        .setCheck("Number")
        .setAlign(Blockly.ALIGN_RIGHT)
        .appendField("Puerto");
    this.appendValueInput("data")
        .setCheck("String")
        .setAlign(Blockly.ALIGN_RIGHT)
        .appendField("Dato a enviar");
    this.setInputsInline(false);
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(160);
 this.setTooltip("Recibir datos por servicio OSC y almacenar en la variable OSC_IN, se requiere configurar un puerto de escucha, para hacer uso de OSC_IN buscar el bloque correspondiente en el toolbox.");
 this.setHelpUrl("");
  }
};

Blockly.Lua['service_osc_send'] = function(block) {
  var value_ip = Blockly.Lua.valueToCode(block, 'ip', Blockly.Lua.ORDER_ATOMIC);
  var value_port = Blockly.Lua.valueToCode(block, 'port', Blockly.Lua.ORDER_ATOMIC);
  var value_data = Blockly.Lua.valueToCode(block, 'data', Blockly.Lua.ORDER_ATOMIC);
  // TODO: Assemble Lua into code variable.
  var code = "sk_osc:send(" + value_port + ", " + value_ip + ", " + value_data + ")\n";
  return code;
};

Blockly.Blocks['osc_format_iiff'] = {
  init: function() {
    this.appendDummyInput()
        .setAlign(Blockly.ALIGN_CENTRE)
        .appendField("Formatear OSC");
    this.appendValueInput("address")
        .setCheck("String")
        .setAlign(Blockly.ALIGN_RIGHT)
        .appendField("Dirección");
    this.appendValueInput("int1")
        .setCheck("Number")
        .setAlign(Blockly.ALIGN_RIGHT)
        .appendField("1er entero");
    this.appendValueInput("int2")
        .setCheck("Number")
        .setAlign(Blockly.ALIGN_RIGHT)
        .appendField("2do entero");
    this.appendValueInput("float1")
        .setCheck("Number")
        .setAlign(Blockly.ALIGN_RIGHT)
        .appendField("1er decimal");
    this.appendValueInput("float2")
        .setCheck("Number")
        .setAlign(Blockly.ALIGN_RIGHT)
        .appendField("2do decimal");
    this.setInputsInline(false);
    this.setOutput(true, null);
    this.setColour(160);
 this.setTooltip("Este bloque permite dar formato a los datos que se desean enviar, siempre se requiere enviar la dirección, la dirección es un texto, por ejemplo: /piano, lo anterior indica que los datos son enviados a un piano. Use los datos que requiera enviar (no necesita llenarlos todos), cuando los reciba en el otro dispositivo, tenga encuenta el orden de los datos para poderlos usar y que tengan sentido.");
 this.setHelpUrl("");
  }
};

Blockly.Lua['osc_format_iiff'] = function(block) {
  var value_address = Blockly.Lua.valueToCode(block, 'address', Blockly.Lua.ORDER_ATOMIC);
  var value_int1 = Blockly.Lua.valueToCode(block, 'int1', Blockly.Lua.ORDER_ATOMIC);
  var value_int2 = Blockly.Lua.valueToCode(block, 'int2', Blockly.Lua.ORDER_ATOMIC);
  var value_float1 = Blockly.Lua.valueToCode(block, 'float1', Blockly.Lua.ORDER_ATOMIC);
  var value_float2 = Blockly.Lua.valueToCode(block, 'float2', Blockly.Lua.ORDER_ATOMIC);
  // TODO: Assemble Lua into code variable.
  var code = '...';
  var code = "osc.set_iiff(" + value_address + ", " + value_int1 + ", " + value_int2 + ", " + value_float1 + ", " + value_float2 + ")";
  // TODO: Change ORDER_NONE to the correct strength.
  return [code, Blockly.Lua.ORDER_NONE];
};

Blockly.Blocks['osc_format_ifs'] = {
  init: function() {
    this.appendDummyInput()
        .setAlign(Blockly.ALIGN_CENTRE)
        .appendField("Formatear OSC");
    this.appendValueInput("address")
        .setCheck("String")
        .setAlign(Blockly.ALIGN_RIGHT)
        .appendField("Dirección");
    this.appendValueInput("integer")
        .setCheck("Number")
        .setAlign(Blockly.ALIGN_RIGHT)
        .appendField("Entero");
    this.appendValueInput("float")
        .setCheck("Number")
        .setAlign(Blockly.ALIGN_RIGHT)
        .appendField("Decimal");
    this.appendValueInput("text")
        .setCheck("String")
        .setAlign(Blockly.ALIGN_RIGHT)
        .appendField("Texto");
    this.setInputsInline(false);
    this.setOutput(true, null);
    this.setColour(160);
 this.setTooltip("Este bloque permite dar formato a los datos que se desean enviar, siempre se requiere enviar la dirección, la dirección es un texto, por ejemplo: /piano, lo anterior indica que los datos son enviados a un piano. Use los datos que requiera enviar (no necesita llenarlos todos), cuando los reciba en el otro dispositivo, tenga encuenta el orden de los datos para poderlos usar y que tengan sentido.");
 this.setHelpUrl("");
  }
};

Blockly.Lua['osc_format_ifs'] = function(block) {
  var value_address = Blockly.Lua.valueToCode(block, 'address', Blockly.Lua.ORDER_ATOMIC);
  var value_integer = Blockly.Lua.valueToCode(block, 'integer', Blockly.Lua.ORDER_ATOMIC);
  var value_float = Blockly.Lua.valueToCode(block, 'float', Blockly.Lua.ORDER_ATOMIC);
  var value_text = Blockly.Lua.valueToCode(block, 'text', Blockly.Lua.ORDER_ATOMIC);
  // TODO: Assemble Lua into code variable.
  var code = "osc.set_ifs(" + value_address + ", " + value_integer + ", " + value_float + ", " + value_text + ")";
  // TODO: Change ORDER_NONE to the correct strength.
  return [code, Blockly.Lua.ORDER_NONE];
};

