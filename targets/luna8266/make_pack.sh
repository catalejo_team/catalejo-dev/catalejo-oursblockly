#!/bin/bash -e
#
# Brief:	Empaquetador de archivos.
# Author: Johnny Cubides
# e-mail: jgcubidesc@gmail.com, johnnycubides@catalejoplus.com
# date: Sunday 03 February 2019

# foreground
BLACK=$(tput setaf 0)
RED=$(tput setaf 1)
GREEN=$(tput setaf 2)
YELLOW=$(tput setaf 3)
BLUE=$(tput setaf 4)
MAGENTA=$(tput setaf 5)
CYAN=$(tput setaf 6)
WHITE=$(tput setaf 7)
NC=$(tput setaf 7)
# background
BLACKB=$(tput setab 0)
REDB=$(tput setab 1)
GREENB=$(tput setab 2)
YELLOWB=$(tput setab 3)
BLUEB=$(tput setab 4)
MAGENTAB=$(tput setab 5)
CYANB=$(tput setab 6)
WHITEB=$(tput setab 7)
NCB=$(tput setab 0)

TARGET=${2:-usermodules}.js

DIST=${2:-dist}
DIST=dist/$DIST
NAME_VAR_TOOLBOX="luna8266Toolbox"
PYTHON=python3

function general() {
	mkdir -p $DIST

	rm -rf $DIST/*
	java -jar ../../closure-compiler.jar --js *.js --js_output_file $DIST/$TARGET

	cp -r imag $DIST

	$PYTHON toolbox.py "export" $NAME_VAR_TOOLBOX
	# python toolbox.py "no export"

	mv toolbox.js toolbox.d.ts $DIST
}

function webtest() {
	# Remove export incompatible with web
	FIND=export
	REPLACE=
	sed -i "s!$FIND!$REPLACE!" $DIST/toolbox.js

	# Update image path 1
	FIND=./path_img/
	REPLACE=../catalejo-oursblockly/targets/luna8266/dist/luna8266/imag/
	sed -i "s!$FIND!$REPLACE!g" $DIST/$TARGET

	# Update img path 2
	FIND=dependencies/modulesNodeMCU/javascript/imag/
	REPLACE=../catalejo-oursblockly/targets/luna8266/dist/luna8266/imag/
	sed -i "s!$FIND!$REPLACE!g" $DIST/$TARGET
	echo "El resultado es compatible con luna8266.html en la ruta:"
	echo "../../../test-html/luna8266.html del proyecto catalejo-blockly"
}

function desktop() {
	# FIND='FieldImage("dependencies/modulesNodeMCU/javascript/imag/'
	# REPLACE='FieldImage("./modes/default/blockly/imag/'
	# sed -i \'s/$FIND/$REPLACE/g\' $DIST/$TARGET
	# sed -i 's/"dependencies\/modulesNodeMCU\/javascript\/imag\//"\.\/modes\/luabot\/blockly\/imag\//g' $DIST/$TARGET
	# FIND=dependencies/modulesNodeMCU/javascript/imag/
	# REPLACE=./modes/default/blockly/imag/

	# Actualizando img path 1 TODO deberá ponerse en obsoleto
	FIND=./path_img/
	REPLACE=./dist/img/imag/
	sed -i "s!$FIND!$REPLACE!g" $DIST/$TARGET

	# Actualizando img path 2
	FIND=dependencies/modulesNodeMCU/javascript/imag/
	REPLACE=./dist/img/imag/
	sed -i "s!$FIND!$REPLACE!g" $DIST/$TARGET

	# Acondicionando nombres de librerías para la versión de escritorio
	FIND=Blockly.Lua
	REPLACE=luaGenerator
	sed -i "s!$FIND!$REPLACE!g" $DIST/$TARGET

	# Agregar las siguientes líneas al principio del target
	sed -i "1i import * as Blockly from \"@johnnycubides/blockly-catalejo/\";" $DIST/$TARGET
	sed -i "1i import { luaGenerator } from \"@johnnycubides/blockly-catalejo/lua\";" $DIST/$TARGET
	echo "export { luaGenerator };" >>$DIST/$TARGET
}

function mobile() {
	# FIND='FieldImage("dependencies/modulesNodeMCU/javascript/imag/'
	# REPLACE='FieldImage("./modes/default/blockly/imag/'
	# sed -i \'s/$FIND/$REPLACE/g\' $DIST/$TARGET
	# sed -i 's/"dependencies\/modulesNodeMCU\/javascript\/imag\//"\.\/modes\/luabot\/blockly\/imag\//g' $DIST/$TARGET
	# Remplazar por requerimiento específico

	# Actualizar img path 1
	FIND=dependencies/modulesNodeMCU/javascript/imag/
	REPLACE=./img/imag/
	sed -i "s!$FIND!$REPLACE!g" $DIST/$TARGET

	# Actualizando img path 2
	FIND=./path_img/
	REPLACE=./img/imag/
	sed -i "s!$FIND!$REPLACE!g" $DIST/$TARGET

	# Se requiere agregar esta linea para poder importar correctamente
	sed -i "1i import Blockly from \'blockly\';" $DIST/$TARGET

	# TODO estas líneas son para la compatibilidad del blocklyu 5.2021
	# en catalejo editor mobile, es necesario actualizar el webview de
	# la aplicación y quitar estar líneas
	FIND=CATEGORY_NAME
	REPLACE=NAME_TYPE
	sed -i "s!$FIND!$REPLACE!g" $DIST/$TARGET
	FIND=nameDB_
	REPLACE=variableDB_
	sed -i "s!$FIND!$REPLACE!g" $DIST/$TARGET
}

# Permite guardar información acerca de comandos usados
if [ "$1" = "-h" ] || [ "$1" = "" ] || [ "$1" = "--help" ]; then
	printf "Help for this command make_pack.sh\n"
	printf "\t${CYAN}make_pack.sh${NC} Command options\n"
	printf "\t[Commands]\n"
	printf "\t\t${CYAN}webtest${NC} ${YELLOW}luna8266${NC}\tpack for javascript (test-html)\n"
	printf "\t\t${CYAN}desktop${NC} ${YELLOW}luna8266${NC}\tpack for catalejo-editor\n"
	printf "\t\t${CYAN}mobile${NC} ${YELLOW}luna8266${NC}\tpack for mobile app\n"
	printf "\t\t${CYAN}mobile${NC} ${YELLOW}luabot${NC}\tpack for mobile app\n"
	printf "\t\t-h,--help\tHelp\n"
	printf "\n${GREEN}Regards Johnny.${NC}\n"
elif [ "$1" = "mobile" ]; then
	# TODO: Se requiere borrar esta opción cuando sea actializado
	# webview de la aplicación
	NAME_VAR_TOOLBOX="toolbox"
	general "$1"
	mobile
elif [ "$1" = "desktop" ]; then
	general "$1"
	desktop
elif [ "$1" = "webtest" ]; then
	general "$1"
	webtest
fi
