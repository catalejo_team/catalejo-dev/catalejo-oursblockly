/* INIT SERVER SOCKET */

// CREATE SERVER
Blockly.Blocks['init_tcp'] = {
  init: function() {
    this.appendValueInput("timeout")
        .setCheck("Number")
        .appendField("Crear servidor")
        .appendField(new Blockly.FieldDropdown([["TCP", "net.TCP"], ["UPD", "net.UDP"]]), "type")
        .appendField("timeout");
    this.setInputsInline(true);
    this.setOutput(true, null);
    this.setColour(265);
    this.setTooltip('');
    this.setHelpUrl('http://www.example.com/');
  }
};

Blockly.Lua['init_tcp'] = function(block) {
  var dropdown_type = block.getFieldValue('type');
  var value_timeout = Blockly.Lua.valueToCode(block, 'timeout', Blockly.Lua.ORDER_ATOMIC);
  // TODO: Assemble Lua into code variable.
  var code = `net.createServer(${dropdown_type}, ${value_timeout})
  `;
  // TODO: Change ORDER_NONE to the correct strength.
  return [code, Blockly.Lua.ORDER_NONE];
};

// TCP LISTEN
Blockly.Blocks['tcp_listen'] = {
  init: function() {
    this.appendValueInput("var_port")
        .setCheck(null)
        .appendField(new Blockly.FieldVariable("item"), "server_tcp")
        .appendField(", port");
    this.appendDummyInput()
        .appendField("sck, datos");
    this.appendStatementInput("callback")
        .setCheck(null);
    this.setInputsInline(true);
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(265);
    this.setTooltip('');
    this.setHelpUrl('http://www.example.com/');
  }
};

Blockly.Lua['tcp_listen'] = function(block) {
  var variable_server_tcp = Blockly.Lua.nameDB_.getName(block.getFieldValue('server_tcp'), Blockly.Variables.CATEGORY_NAME);
  var value_var_port = Blockly.Lua.valueToCode(block, 'var_port', Blockly.Lua.ORDER_ATOMIC);
  var statements_callback = Blockly.Lua.statementToCode(block, 'callback');
  // TODO: Assemble Lua into code variable.
  var code = `if ${variable_server_tcp} then
  ${variable_server_tcp}:listen(${value_var_port}, function(sck)
    ${statements_callback} end)
end
`;
  return code;
};

// TCP EVENTS
Blockly.Blocks['tcp_events'] = {
  init: function() {
    this.appendDummyInput()
        .appendField(new Blockly.FieldDropdown([["enviado", "sent"], ["recibido", "receive"], ["conectado", "conecction"], ["desconectado", "disconnection"], ["reconectado", "reconnection"]]), "events")
        .appendField("datos");
    this.appendStatementInput("callback")
        .setCheck(null);
    this.setInputsInline(true);
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(265);
    this.setTooltip('');
    this.setHelpUrl('http://www.example.com/');
  }
};

Blockly.Lua['tcp_events'] = function(block) {
  var dropdown_events = block.getFieldValue('events');
  var statements_callback = Blockly.Lua.statementToCode(block, 'callback');
  // TODO: Assemble Lua into code variable.
  var code = `sck:on("${dropdown_events}", function(sck, datos)
${statements_callback}end)
`;
  return code;
};

// data receiver
Blockly.Blocks['tcp_data'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("dato sck");
    this.setOutput(true, null);
    this.setColour(265);
    this.setTooltip('');
    this.setHelpUrl('http://www.example.com/');
  }
};

Blockly.Lua['tcp_data'] = function(block) {
  // TODO: Assemble Lua into code variable.
  var code = `datos`;
  // TODO: Change ORDER_NONE to the correct strength.
  return [code, Blockly.Lua.ORDER_NONE];
};

// data send
Blockly.Blocks['tcp_send'] = {
  init: function() {
    this.appendValueInput("send_data")
        .setCheck(null)
        .appendField("sck enviar dato");
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(265);
    this.setTooltip('');
    this.setHelpUrl('http://www.example.com/');
  }
};

Blockly.Lua['tcp_send'] = function(block) {
  var value_send_data = Blockly.Lua.valueToCode(block, 'send_data', Blockly.Lua.ORDER_ATOMIC);
  // TODO: Assemble Lua into code variable.
  var code = `sck:send(${value_send_data})
`;
  return code;
};
