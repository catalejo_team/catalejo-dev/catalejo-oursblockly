/*
 * SERVO MOTOR
 */

/*Blockly.Blocks['servo_lib'] = {
  init: function() {
    this.appendDummyInput()
        .setAlign(Blockly.ALIGN_CENTRE)
        .appendField("Librería");
    this.appendDummyInput()
        .setAlign(Blockly.ALIGN_CENTRE)
        .appendField("Servomotor");
    this.appendDummyInput()
        .setAlign(Blockly.ALIGN_CENTRE)
        .appendField(new Blockly.FieldImage("https://learn.digilentinc.com/Documents/chipKIT/P17/servo_schem.svg", 60, 60, "*"));
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(120);
    this.setTooltip('');
    this.setHelpUrl('http://www.example.com/');
  }
};

Blockly.Lua['servo_lib'] = function(block) {
  // TODO: Assemble Lua into code variable.
var code = 'function init_servo(pin,angle)\n'+
'pwm.setup(pin, 123, 81+angle)\n'+
'pwm.start(pin)\n'+'end\n'+
'function angle_servo(pin,angle)\n'+
'pwm.setduty(pin,81+angle)\n'+
'end\n'+
'function off_servo(pin)\n'+
'pwm.stop(pin)\n'+
'end\n';
  return code;
};
*/

Blockly.Blocks['servo_init'] = {
  init: function() {
    this.appendDummyInput()
        .setAlign(Blockly.ALIGN_CENTRE)
        .appendField("Start");
    this.appendDummyInput()
        .setAlign(Blockly.ALIGN_CENTRE)
        .appendField("Servomotor");
    this.appendDummyInput()
        .setAlign(Blockly.ALIGN_CENTRE)
        .appendField(new Blockly.FieldImage("dependencies/modulesNodeMCU/javascript/imag/servo.gif", 40, 40, "*"));
    this.appendValueInput("pin")
        .setCheck("Number")
        .setAlign(Blockly.ALIGN_RIGHT)
        .appendField("Pin");
    this.appendValueInput("angle")
        .setCheck("Number")
        .setAlign(Blockly.ALIGN_RIGHT)
        .appendField("Ángulo inicial");
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(290);
    this.setTooltip('');
    this.setHelpUrl('http://www.example.com/');
  }
};

Blockly.Lua['servo_init'] = function(block) {
  var value_pin = Blockly.Lua.valueToCode(block, 'pin', Blockly.Lua.ORDER_ATOMIC);
  var value_angle = Blockly.Lua.valueToCode(block, 'angle', Blockly.Lua.ORDER_ATOMIC);
  // TODO: Assemble Lua into code variable.
  var code = `pwm.setup(${value_pin}, 123, 81+${value_angle})
pwm.start(${value_pin})
`;
  return code;
};

Blockly.Blocks['servo_angle'] = {
  init: function() {
    this.appendDummyInput()
        .setAlign(Blockly.ALIGN_CENTRE)
        .appendField("Cambiar Ángulo");
    this.appendDummyInput()
        .setAlign(Blockly.ALIGN_CENTRE)
        .appendField("Servomotor");
    this.appendDummyInput()
        .setAlign(Blockly.ALIGN_CENTRE)
        .appendField(new Blockly.FieldImage("dependencies/modulesNodeMCU/javascript/imag/servo.gif", 40, 40, "*"));
    this.appendValueInput("pin")
        .setCheck("Number")
        .setAlign(Blockly.ALIGN_RIGHT)
        .appendField("Pin");
    this.appendValueInput("angle")
        .setCheck("Number")
        .setAlign(Blockly.ALIGN_RIGHT)
        .appendField("Nuevo ángulo");
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(290);
    this.setTooltip('');
    this.setHelpUrl('http://www.example.com/');
  }
};

Blockly.Lua['servo_angle'] = function(block) {
  var value_pin = Blockly.Lua.valueToCode(block, 'pin', Blockly.Lua.ORDER_ATOMIC);
  var value_angle = Blockly.Lua.valueToCode(block, 'angle', Blockly.Lua.ORDER_ATOMIC);
  // TODO: Assemble Lua into code variable.
  var code = `pwm.setduty(${value_pin},81+${value_angle})
`;
  return code;
};

Blockly.Blocks['servo_stop'] = {
  init: function() {
    this.appendDummyInput()
        .setAlign(Blockly.ALIGN_CENTRE)
        .appendField("Detener");
    this.appendDummyInput()
        .setAlign(Blockly.ALIGN_CENTRE)
        .appendField("Servomotor");
    this.appendDummyInput()
        .setAlign(Blockly.ALIGN_CENTRE)
        .appendField(new Blockly.FieldImage("dependencies/modulesNodeMCU/javascript/imag/servo.gif", 40, 40, "*"));
    this.appendValueInput("pin")
        .setCheck("Number")
        .setAlign(Blockly.ALIGN_RIGHT)
        .appendField("Pin");
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(290);
    this.setTooltip('');
    this.setHelpUrl('http://www.example.com/');
  }
};

Blockly.Lua['servo_stop'] = function(block) {
  var value_pin = Blockly.Lua.valueToCode(block, 'pin', Blockly.Lua.ORDER_ATOMIC);
  // TODO: Assemble Lua into code variable.
  var code = `pwm.stop(${value_pin})
`;
  return code;
};

/* cambios para ws2812
Blockly.Blocks['ws2812_init'] = {
  init: function() {
    this.appendDummyInput()
        .setAlign(Blockly.ALIGN_CENTRE)
        .appendField(new Blockly.FieldImage("https://www.gstatic.com/codesite/ph/images/star_on.gif", 15, 15, "*"));
    this.appendDummyInput()
        .setAlign(Blockly.ALIGN_CENTRE)
        .appendField("iniciar ws2812");
    this.appendDummyInput()
        .setAlign(Blockly.ALIGN_CENTRE)
        .appendField(new Blockly.FieldDropdown([["simple (D4)", "OPTIONNAME"], ["dual (D4, TX)", "ws2812.MODE_DUAL"], ["option", "ws2812.MODE_SINGLE"]]), "modo");
    this.setColour(210);
    this.setTooltip('');
    this.setHelpUrl('http://www.example.com/');
  }
};

Blockly.Lua['ws2812_init'] = function(block) {
  var dropdown_modo = block.getFieldValue('modo');
  // TODO: Assemble Lua into code variable.
  var code = '...\n';
  return code;
};
*/

// ws2812
// Blockly.Blocks['ws2812_init'] = {
//   init: function() {
//     this.appendDummyInput()
//         .setAlign(Blockly.ALIGN_CENTRE)
//         .appendField(new Blockly.FieldImage("dependencies/modulesNodeMCU/javascript/imag/ws2812.png", 30, 30, "*"));
//     this.appendDummyInput()
//         .appendField("Iniciar ws2812");
//     this.appendDummyInput()
//         .setAlign(Blockly.ALIGN_CENTRE)
//         .appendField("pin D4 (gpio2)");
//     this.setInputsInline(false);
//     this.setPreviousStatement(true, null);
//     this.setNextStatement(true, null);
//     this.setColour(210);
//     this.setTooltip('');
//     this.setHelpUrl('http://www.example.com/');
//   }
// };

// Blockly.Lua['ws2812_init'] = function(block) {
//   // TODO: Assemble Lua into code variable.
//   var code = `ws2812.init()
// `;
//   return code;
// };

Blockly.Blocks['ws2812_init'] = {
  init: function() {
    this.appendDummyInput()
        .setAlign(Blockly.ALIGN_CENTRE)
        .appendField(new Blockly.FieldImage("dependencies/modulesNodeMCU/javascript/imag/ws2812.png", 30, 30, "*"));
    this.appendDummyInput()
        .setAlign(Blockly.ALIGN_CENTRE)
        .appendField("Iniciar ws2812");
    this.appendDummyInput()
        .setAlign(Blockly.ALIGN_CENTRE)
        .appendField(new Blockly.FieldDropdown([["Simple pin:D4", "ws2812.MODE_SINGLE"], ["Dual pin:D4, TX", "ws2812.MODE_DUAL"]]), "mode");
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(290);
    this.setTooltip('');
    this.setHelpUrl('http://www.example.com/');
  }
};

Blockly.Lua['ws2812_init'] = function(block) {
  var dropdown_mode = block.getFieldValue('mode');
  // TODO: Assemble Lua into code variable.
  var code = `ws2812.init(${dropdown_mode})
`;
  return code;
};

Blockly.Blocks['ws2812_format'] = {
  init: function() {
    this.appendStatementInput("format")
        .setCheck(null)
        .appendField(new Blockly.FieldImage("https://http2.mlstatic.com/D_NQ_NP_718805-MLA31044696746_062019-Q.jpg", 15, 15, "*"))
        .appendField("format");
    this.setInputsInline(true);
    this.setOutput(true, null);
    this.setColour(290);
    this.setTooltip('');
    this.setHelpUrl('http://www.example.com/');
  }
};
Blockly.Lua['ws2812_format'] = function(block) {
  var statements_format = Blockly.Lua.statementToCode(block, 'format');
  // TODO: Assemble Lua into code variable.
  var code = `string.char(${statements_format})`;
  // TODO: Change ORDER_NONE to the correct strength.
  return [code, Blockly.Lua.ORDER_NONE];
};

Blockly.Blocks['ws2812_array'] = {
  init: function() {
    this.appendValueInput("R")
        .setCheck("Number")
        .setAlign(Blockly.ALIGN_CENTRE)
      .appendField(new Blockly.FieldImage("dependencies/modulesNodeMCU/javascript/imag/ws2812.png", 15, 15, "*"))
        .appendField("R");
    this.appendValueInput("G")
        .setCheck("Number")
        .appendField("G");
    this.appendValueInput("B")
        .setCheck("Number")
        .appendField("B");
    this.setInputsInline(true);
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(290);
    this.setTooltip('');
    this.setHelpUrl('http://www.example.com/');
  }
};

Blockly.Lua['ws2812_array'] = function(block) {
  var value_r = Blockly.Lua.valueToCode(block, 'R', Blockly.Lua.ORDER_ATOMIC);
  var value_g = Blockly.Lua.valueToCode(block, 'G', Blockly.Lua.ORDER_ATOMIC);
  var value_b = Blockly.Lua.valueToCode(block, 'B', Blockly.Lua.ORDER_ATOMIC);
  // TODO: Assemble Lua into code variable.
  var code = `${value_g}, ${value_r}, ${value_b}, `;
  return code;
};

Blockly.Blocks['ws2812_array_finish'] = {
  init: function() {
    this.appendValueInput("R")
        .setCheck("Number")
        .setAlign(Blockly.ALIGN_CENTRE)
      .appendField(new Blockly.FieldImage("dependencies/modulesNodeMCU/javascript/imag/ws2812.png", 15, 15, "*"))
        .appendField("R");
    this.appendValueInput("G")
        .setCheck("Number")
        .appendField("G");
    this.appendValueInput("B")
        .setCheck("Number")
        .appendField("B");
    this.setInputsInline(true);
    this.setPreviousStatement(true, null);
    this.setNextStatement(false, null);
    this.setColour(290);
    this.setTooltip('');
    this.setHelpUrl('http://www.example.com/');
  }
};

Blockly.Lua['ws2812_array_finish'] = function(block) {
  var value_r = Blockly.Lua.valueToCode(block, 'R', Blockly.Lua.ORDER_ATOMIC);
  var value_g = Blockly.Lua.valueToCode(block, 'G', Blockly.Lua.ORDER_ATOMIC);
  var value_b = Blockly.Lua.valueToCode(block, 'B', Blockly.Lua.ORDER_ATOMIC);
  // TODO: Assemble Lua into code variable.
  var code = `${value_g}, ${value_r}, ${value_b}`;
  return code;
};

Blockly.Blocks['ws2812_write'] = {
  init: function() {
    this.appendValueInput("write")
        .setCheck("String")
        .setAlign(Blockly.ALIGN_CENTRE)
        .appendField(new Blockly.FieldImage("dependencies/modulesNodeMCU/javascript/imag/ws2812.png", 15, 15, "*"))
        .appendField("escribir (D4)");
    this.setInputsInline(false);
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(290);
    this.setTooltip('');
    this.setHelpUrl('http://www.example.com/');
  }
};

Blockly.Lua['ws2812_write'] = function(block) {
  var value_write = Blockly.Lua.valueToCode(block, 'write', Blockly.Lua.ORDER_ATOMIC);
  // TODO: Assemble Lua into code variable.
  var code = `ws2812.write(${value_write})
`;
  return code;
};

Blockly.Blocks['ws2812_write_dual'] = {
  init: function() {
    this.appendDummyInput()
        .setAlign(Blockly.ALIGN_CENTRE)
        .appendField(new Blockly.FieldImage("dependencies/modulesNodeMCU/javascript/imag/ws2812.png", 15, 15, "*"))
    this.appendDummyInput()
        .setAlign(Blockly.ALIGN_CENTRE)
        .appendField("Escribir");
    this.appendValueInput("write d4")
        .setCheck(null)
        .setAlign(Blockly.ALIGN_RIGHT)
        .appendField("en D4");
    this.appendValueInput("write tx")
        .setCheck(null)
        .setAlign(Blockly.ALIGN_RIGHT)
        .appendField("en TX");
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(290);
    this.setTooltip('');
    this.setHelpUrl('http://www.example.com/');
  }
};

Blockly.Lua['ws2812_write_dual'] = function(block) {
  var value_write_d4 = Blockly.Lua.valueToCode(block, 'write d4', Blockly.Lua.ORDER_ATOMIC);
  var value_write_tx = Blockly.Lua.valueToCode(block, 'write tx', Blockly.Lua.ORDER_ATOMIC);
  // TODO: Assemble Lua into code variable.
  value_write_d4 = (value_write_d4 == "")?"nil": value_write_d4
  value_write_tx = (value_write_tx == "")?"nil": value_write_tx
  var code = `ws2812.write(${value_write_d4}, ${value_write_tx})
`;
  return code;
};

// DISPLAY OLED
Blockly.Blocks['display_oled_init'] = {
  init: function() {
    this.appendDummyInput()
        .setAlign(Blockly.ALIGN_CENTRE)
        .appendField(new Blockly.FieldImage("dependencies/modulesNodeMCU/javascript/imag/oled.jpg", 30, 30, "*"));
    this.appendDummyInput()
        .setAlign(Blockly.ALIGN_RIGHT)
        .appendField(new Blockly.FieldVariable("item"), "disp");
    this.appendValueInput("id")
        .setCheck(null)
        .setAlign(Blockly.ALIGN_RIGHT)
        .appendField("id:");
    this.appendValueInput("sda")
        .setCheck(null)
        .setAlign(Blockly.ALIGN_RIGHT)
        .appendField("sda:");
    this.appendValueInput("scl")
        .setCheck(null)
        .setAlign(Blockly.ALIGN_RIGHT)
        .appendField("scl:");
    this.appendValueInput("sla")
        .setCheck(null)
        .setAlign(Blockly.ALIGN_RIGHT)
        .appendField("sla:");
    this.appendDummyInput()
        .setAlign(Blockly.ALIGN_RIGHT)
        .appendField("velocidad:");
    this.appendDummyInput()
        .setAlign(Blockly.ALIGN_RIGHT)
        .appendField(new Blockly.FieldDropdown([["lento", "i2c.SLOW"], ["rápido", "i2c.FAST"], ["muy rápido", "i2c.FASTPLUS"]]), "speed");
    this.setInputsInline(false);
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(290);
    this.setTooltip('');
    this.setHelpUrl('http://www.example.com/');
  }
};

Blockly.Lua['display_oled_init'] = function(block) {
  var variable_disp = Blockly.Lua.nameDB_.getName(block.getFieldValue('disp'), Blockly.Variables.CATEGORY_NAME);
  var value_id = Blockly.Lua.valueToCode(block, 'id', Blockly.Lua.ORDER_ATOMIC);
  var value_sda = Blockly.Lua.valueToCode(block, 'sda', Blockly.Lua.ORDER_ATOMIC);
  var value_scl = Blockly.Lua.valueToCode(block, 'scl', Blockly.Lua.ORDER_ATOMIC);
  var value_sla = Blockly.Lua.valueToCode(block, 'sla', Blockly.Lua.ORDER_ATOMIC);
  var dropdown_speed = block.getFieldValue('speed');
  // TODO: Assemble Lua into code variable.
  var code = 'i2c.setup('+value_id+', '+value_sda+', '+value_scl+', '+dropdown_speed+')\n'
    +variable_disp+' = u8g2.ssd1306_i2c_128x64_noname('+value_id+', '+value_sla+')\n'
    +variable_disp+':setFont(u8g2.font_6x10_tf)\n'
    +variable_disp+':setFontRefHeightExtendedText()\n'
    +variable_disp+':setDrawColor(1)\n'
    +variable_disp+':setFontPosTop()\n';
  return code;
};

Blockly.Blocks['display_oled_buffer'] = {
  init: function() {
    this.appendDummyInput()
        .setAlign(Blockly.ALIGN_CENTRE)
        .appendField(new Blockly.FieldImage("dependencies/modulesNodeMCU/javascript/imag/oled.jpg", 15, 15, "*"));
    this.appendDummyInput()
        .setAlign(Blockly.ALIGN_RIGHT)
        .appendField(new Blockly.FieldVariable("item"), "disp");
    this.appendDummyInput()
        .setAlign(Blockly.ALIGN_RIGHT)
        .appendField(":")
        .appendField(new Blockly.FieldDropdown([["escribir_en_pantalla", "sendBuffer()"], ["limpiar_pantalla", "clearBuffer()"]]), "action");
    this.setInputsInline(true);
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(290);
    this.setTooltip('');
    this.setHelpUrl('http://www.example.com/');
  }
};

Blockly.Lua['display_oled_buffer'] = function(block) {
  var variable_disp = Blockly.Lua.nameDB_.getName(block.getFieldValue('disp'), Blockly.Variables.CATEGORY_NAME);
  var dropdown_action = block.getFieldValue('action');
  // TODO: Assemble Lua into code variable.
  var code = variable_disp+':'+dropdown_action+'\n';
  return code;
};

Blockly.Blocks['display_oled_angle_text'] = {
  init: function() {
    this.appendDummyInput()
        .setAlign(Blockly.ALIGN_CENTRE)
        .appendField(new Blockly.FieldImage("dependencies/modulesNodeMCU/javascript/imag/oled.jpg", 15, 15, "*"));
    this.appendDummyInput()
        .setAlign(Blockly.ALIGN_RIGHT)
        .appendField(new Blockly.FieldVariable("item"), "disp");
    this.appendDummyInput()
        .setAlign(Blockly.ALIGN_RIGHT)
        .appendField(":posición del texto")
        .appendField(new Blockly.FieldDropdown([["0º", "0"], ["90º", "3"], ["180º", "2"], ["270º", "1"]]), "action");
    this.setInputsInline(true);
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(290);
    this.setTooltip('');
    this.setHelpUrl('http://www.example.com/');
  }
};

Blockly.Lua['display_oled_angle_text'] = function(block) {
  var variable_disp = Blockly.Lua.nameDB_.getName(block.getFieldValue('disp'), Blockly.Variables.CATEGORY_NAME);
  var dropdown_action = block.getFieldValue('action');
  // TODO: Assemble Lua into code variable.
  var code = variable_disp+':setFontDirection('+dropdown_action+')\n';
  return code;
};

Blockly.Blocks['display_oled_str'] = {
  init: function() {
    this.appendDummyInput()
        .setAlign(Blockly.ALIGN_CENTRE)
        .appendField(new Blockly.FieldImage("dependencies/modulesNodeMCU/javascript/imag/oled.jpg", 15, 15, "*"));
    this.appendDummyInput()
        .setAlign(Blockly.ALIGN_RIGHT)
        .appendField(new Blockly.FieldVariable("item"), "disp");
    this.appendValueInput("text")
        .setCheck(null)
        .setAlign(Blockly.ALIGN_RIGHT)
        .appendField("texto:");
    this.appendValueInput("x0")
        .setCheck("Number")
        .setAlign(Blockly.ALIGN_RIGHT)
        .appendField("x0:");
    this.appendValueInput("y0")
        .setCheck("Number")
        .setAlign(Blockly.ALIGN_RIGHT)
        .appendField("y0:");
    this.setInputsInline(false);
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(290);
    this.setTooltip('');
    this.setHelpUrl('http://www.example.com/');
  }
};

Blockly.Lua['display_oled_str'] = function(block) {
  var variable_disp = Blockly.Lua.nameDB_.getName(block.getFieldValue('disp'), Blockly.Variables.CATEGORY_NAME);
  var value_text = Blockly.Lua.valueToCode(block, 'text', Blockly.Lua.ORDER_ATOMIC);
  var value_x0 = Blockly.Lua.valueToCode(block, 'x0', Blockly.Lua.ORDER_ATOMIC);
  var value_y0 = Blockly.Lua.valueToCode(block, 'y0', Blockly.Lua.ORDER_ATOMIC);
  // TODO: Assemble Lua into code variable.
  var code = variable_disp+':drawStr('+value_x0+', '+value_y0+', '+value_text+')\n';
  return code;
};

Blockly.Blocks['display_oled_circ_disc'] = {
  init: function() {
    this.appendDummyInput()
        .setAlign(Blockly.ALIGN_CENTRE)
        .appendField(new Blockly.FieldImage("dependencies/modulesNodeMCU/javascript/imag/oled.jpg", 15, 15, "*"));
    this.appendDummyInput()
        .setAlign(Blockly.ALIGN_RIGHT)
        .appendField(new Blockly.FieldVariable("item"), "disp");
    this.appendDummyInput()
        .setAlign(Blockly.ALIGN_CENTRE)
        .appendField(new Blockly.FieldDropdown([["círculo", "drawCircle"], ["disco", "drawDisc"]]), "draw");
    this.appendValueInput("x0")
        .setCheck("Number")
        .setAlign(Blockly.ALIGN_RIGHT)
        .appendField("x0:");
    this.appendValueInput("y0")
        .setCheck("Number")
        .setAlign(Blockly.ALIGN_RIGHT)
        .appendField("y0:");
    this.appendValueInput("radio")
        .setCheck("Number")
        .setAlign(Blockly.ALIGN_RIGHT)
        .appendField("radio:");
    this.setInputsInline(false);
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(290);
    this.setTooltip('');
    this.setHelpUrl('http://www.example.com/');
  }
};

Blockly.Lua['display_oled_circ_disc'] = function(block) {
  var variable_disp = Blockly.Lua.nameDB_.getName(block.getFieldValue('disp'), Blockly.Variables.CATEGORY_NAME);
  var dropdown_draw = block.getFieldValue('draw');
  var value_x0 = Blockly.Lua.valueToCode(block, 'x0', Blockly.Lua.ORDER_ATOMIC);
  var value_y0 = Blockly.Lua.valueToCode(block, 'y0', Blockly.Lua.ORDER_ATOMIC);
  var value_radio = Blockly.Lua.valueToCode(block, 'radio', Blockly.Lua.ORDER_ATOMIC);
  // TODO: Assemble Lua into code variable.
  var code = variable_disp+':'+dropdown_draw+'('+value_x0+', '+value_y0+', '+value_radio+')\n';
  return code;
};

Blockly.Blocks['display_oled_frame_box'] = {
  init: function() {
    this.appendDummyInput()
        .setAlign(Blockly.ALIGN_CENTRE)
        .appendField(new Blockly.FieldImage("dependencies/modulesNodeMCU/javascript/imag/oled.jpg", 15, 15, "*"));
    this.appendDummyInput()
        .setAlign(Blockly.ALIGN_RIGHT)
        .appendField(new Blockly.FieldVariable("item"), "disp");
    this.appendDummyInput()
        .setAlign(Blockly.ALIGN_CENTRE)
        .appendField(new Blockly.FieldDropdown([["marco", "drawFrame"], ["caja", "drawBox"]]), "draw");
    this.appendValueInput("x0")
        .setCheck("Number")
        .setAlign(Blockly.ALIGN_RIGHT)
        .appendField("x0:");
    this.appendValueInput("y0")
        .setCheck("Number")
        .setAlign(Blockly.ALIGN_RIGHT)
        .appendField("y0:");
    this.appendValueInput("width")
        .setCheck("Number")
        .setAlign(Blockly.ALIGN_RIGHT)
        .appendField("ancho:");
    this.appendValueInput("height")
        .setCheck("Number")
        .setAlign(Blockly.ALIGN_RIGHT)
        .appendField("alto:");
    this.setInputsInline(false);
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(290);
    this.setTooltip('');
    this.setHelpUrl('http://www.example.com/');
  }
};

Blockly.Lua['display_oled_frame_box'] = function(block) {
  var variable_disp = Blockly.Lua.nameDB_.getName(block.getFieldValue('disp'), Blockly.Variables.CATEGORY_NAME);
  var dropdown_draw = block.getFieldValue('draw');
  var value_x0 = Blockly.Lua.valueToCode(block, 'x0', Blockly.Lua.ORDER_ATOMIC);
  var value_y0 = Blockly.Lua.valueToCode(block, 'y0', Blockly.Lua.ORDER_ATOMIC);
  var value_width = Blockly.Lua.valueToCode(block, 'width', Blockly.Lua.ORDER_ATOMIC);
  var value_height = Blockly.Lua.valueToCode(block, 'height', Blockly.Lua.ORDER_ATOMIC);
  // TODO: Assemble Lua into code variable.
  var code = variable_disp+':'+dropdown_draw+'('+value_x0+', '+value_y0+', '+value_width+', '+value_height+')\n';
  return code;
}

Blockly.Blocks['display_oled_line'] = {
  init: function() {
    this.appendDummyInput()
        .setAlign(Blockly.ALIGN_CENTRE)
        .appendField(new Blockly.FieldImage("dependencies/modulesNodeMCU/javascript/imag/oled.jpg", 15, 15, "*"));
    this.appendDummyInput()
        .setAlign(Blockly.ALIGN_RIGHT)
        .appendField(new Blockly.FieldVariable("item"), "disp");
    this.appendDummyInput()
        .setAlign(Blockly.ALIGN_CENTRE)
        .appendField("Línea");
    this.appendValueInput("x0")
        .setCheck("Number")
        .setAlign(Blockly.ALIGN_RIGHT)
        .appendField("x0:");
    this.appendValueInput("y0")
        .setCheck("Number")
        .setAlign(Blockly.ALIGN_RIGHT)
        .appendField("y0:");
    this.appendValueInput("x1")
        .setCheck("Number")
        .setAlign(Blockly.ALIGN_RIGHT)
        .appendField("x1:");
    this.appendValueInput("y1")
        .setCheck("Number")
        .setAlign(Blockly.ALIGN_RIGHT)
        .appendField("y1:");
    this.setInputsInline(false);
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(290);
    this.setTooltip('');
    this.setHelpUrl('http://www.example.com/');
  }
};

Blockly.Lua['display_oled_line'] = function(block) {
  var variable_disp = Blockly.Lua.nameDB_.getName(block.getFieldValue('disp'), Blockly.Variables.CATEGORY_NAME);
  var value_x0 = Blockly.Lua.valueToCode(block, 'x0', Blockly.Lua.ORDER_ATOMIC);
  var value_y0 = Blockly.Lua.valueToCode(block, 'y0', Blockly.Lua.ORDER_ATOMIC);
  var value_x1 = Blockly.Lua.valueToCode(block, 'x1', Blockly.Lua.ORDER_ATOMIC);
  var value_y1 = Blockly.Lua.valueToCode(block, 'y1', Blockly.Lua.ORDER_ATOMIC);
  // TODO: Assemble Lua into code variable.
  var code = variable_disp+'drawLine:('+value_x0+', '+value_y0+', '+value_x1+', '+value_y1+')\n';
  return code;
};

// START LED
Blockly.Blocks['led_output'] = {
  init: function() {
    this.appendDummyInput()
        .appendField(new Blockly.FieldImage("./path_img/led.svg", 20, 20, { alt: "*", flipRtl: "FALSE" }))
        .appendField(new Blockly.FieldVariable("item"), "led")
        .appendField("poner a")
        .appendField(new Blockly.FieldDropdown([["ON","gpio.HIGH"], ["OFF","gpio.LOW"], ["TOGGLE","toggle"], ["1","1"], ["0","0"]]), "write");
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(290);
 this.setTooltip("Este bloque permite encender o apagar un led, los posibles valores son: ON y \"1\" para poner el led en alto o 1 lógico, OFF y \"0\" para poner el led a 0 lógico, mientras que TOGGLE intercambia el valor, es decir, si está encendido lo apaga y viceversa.");
 this.setHelpUrl("");
  }
};

Blockly.Blocks['create_led'] = {
  init: function() {
    this.appendValueInput("pin")
        .setCheck("Number")
        .appendField("Crear")
        .appendField(new Blockly.FieldImage("./path_img/led.svg", 20, 20, { alt: "*", flipRtl: "FALSE" }))
        .appendField(new Blockly.FieldVariable("item"), "led")
        .appendField("en");
    this.setInputsInline(false);
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(290);
 this.setTooltip("");
 this.setHelpUrl("Este bloque crea una salida para un LED. en \"item\" deberá crear una variable que represente el LED, las variables se crean en la categoría variable, selecciones también el pin por donde saldrá la señal del LED (D0 hasta D10 o desde 0 hasta 10)");
  }
};

Blockly.Lua['led_output'] = function(block) {
  var variable_led = Blockly.Lua.nameDB_.getName(block.getFieldValue('led'), Blockly.Variables.CATEGORY_NAME);
  var dropdown_write = block.getFieldValue('write');
  // TODO: Assemble Lua into code variable.
  var write_output = "";
  if( dropdown_write == "toggle") {
    write_output = "(gpio.read(" +variable_led + ") == 1 and gpio.LOW or gpio.HIGH)"
  } else {
    write_output = dropdown_write;
  }
  var code = "gpio.write(" + variable_led + ", " + write_output + ")\n";
  return code;
};

Blockly.Lua['create_led'] = function(block) {
  var variable_led = Blockly.Lua.nameDB_.getName(block.getFieldValue('led'), Blockly.Variables.CATEGORY_NAME);
  var value_pin = Blockly.Lua.valueToCode(block, 'pin', Blockly.Lua.ORDER_ATOMIC);
  // TODO: Assemble Lua into code variable.
  var code = variable_led + " = " + value_pin + "\ngpio.mode(" + variable_led + ", gpio.OUTPUT)\n";
  return code;
};

Blockly.Blocks['led_output_ext'] = {
  init: function() {
    this.appendValueInput("led_value")
        .setCheck("Number")
        .appendField(new Blockly.FieldImage("./path_img/led.svg", 20, 20, { alt: "*", flipRtl: "FALSE" }))
        .appendField(new Blockly.FieldVariable("item"), "led")
        .appendField("poner a");
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(290);
 this.setTooltip("Este bloque permite encender o apagar un led, los posibles valores son: 1 para poner el led en alto o 1 lógico, 0 para poner el led a 0 lógico.");
 this.setHelpUrl("");
  }
};

Blockly.Lua['led_output_ext'] = function(block) {
  var variable_led = Blockly.Lua.nameDB_.getName(block.getFieldValue('led'), Blockly.Variables.CATEGORY_NAME);
  var value_led_value = Blockly.Lua.valueToCode(block, 'led_value', Blockly.Lua.ORDER_ATOMIC);
  // TODO: Assemble JavaScript into code variable.
  var code = "gpio.write(" + variable_led + ", " + value_led_value + ")\n";
  return code;
};
