Blockly.Blocks['tmr_var'] = {
  init: function() {
    this.appendValueInput("time")
        .setCheck("Number")
        .appendField("Esperar");
    this.appendDummyInput()
        .appendField(new Blockly.FieldDropdown([["segundos", "*1000000"], ["milisegundos", "*1000"], ["microsegundos", ""]]), "units");
    this.setInputsInline(true);
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(65);
    this.setTooltip('');
    this.setHelpUrl('http://www.example.com/');
  }
};

Blockly.Lua['tmr_var'] = function(block) {
  var value_time = Blockly.Lua.valueToCode(block, 'time', Blockly.Lua.ORDER_ATOMIC);
  var dropdown_units = block.getFieldValue('units');
  // TODO: Assemble Lua into code variable.
  var code = 'tmr.delay('+value_time+dropdown_units+')\n';
  return code;
};

Blockly.Blocks['tmr_s'] = {
  init: function() {
    this.appendValueInput("seconds")
        .setCheck("Number")
        .appendField("Esperar");
    this.appendDummyInput()
        .appendField("(s)");
    this.setInputsInline(true);
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(65);
    this.setTooltip('');
    this.setHelpUrl('http://www.example.com/');
  }
};


Blockly.Lua['tmr_s'] = function(block) {
  var value_seconds = Blockly.Lua.valueToCode(block, 'seconds', Blockly.Lua.ORDER_ATOMIC);
  // TODO: Assemble Lua into code variable.
  var code = 'tmr.delay('+(value_seconds*1000000)+')\n';
  return code;
};

/*
 * tmr_ms
 */
Blockly.Blocks['tmr_ms'] = {
  init: function() {
    this.appendValueInput("miliseconds")
        .setCheck("Number")
        .appendField("Esperar");
    this.appendDummyInput()
        .appendField("(ms)");
    this.setInputsInline(true);
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(65);
    this.setTooltip('');
    this.setHelpUrl('http://www.example.com/');
  }
};

Blockly.Lua['tmr_ms'] = function(block) {                                       
  var value_miliseconds = Blockly.Lua.valueToCode(block, 'miliseconds', Blockly.Lua.ORDER_ATOMIC);                                                              
  // TODO: Assemble Lua into code variable.                                     
  var code = 'tmr.delay('+(value_miliseconds*1000)+')\n';                       
  return code;                                                                  
}; 

/*
 * tmr_us
 */
Blockly.Blocks['tmr_us'] = {
  init: function() {
    this.appendValueInput("microseconds")
        .setCheck("Number")
        .appendField("Esperar");
    this.appendDummyInput()
        .appendField("(us)");
    this.setInputsInline(true);
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(65);
    this.setTooltip('');
    this.setHelpUrl('http://www.example.com/');
  }
};
Blockly.Lua['tmr_us'] = function(block) {
  var value_microseconds = Blockly.Lua.valueToCode(block, 'microseconds', Blockly.Lua.ORDER_ATOMIC);
  // TODO: Assemble Lua into code variable.
  var code = 'tmr.delay('+value_microseconds+')\n';
  return code;
};

// BEGIN TMR ALARM CREATE
Blockly.Blocks['alarm_create'] = {
  init: function() {
    this.appendDummyInput()
        .appendField(new Blockly.FieldImage("dependencies/modulesNodeMCU/javascript/imag/timer.gif", 30, 30, "*"))
        .appendField("Nombre de alarma:")
        .appendField(new Blockly.FieldVariable("mi_alarma"), "name_alarm");
    this.appendValueInput("interval")
        .setCheck("Number")
        .setAlign(Blockly.ALIGN_RIGHT)
        .appendField("Intervalos en ms");
    this.appendStatementInput("NAME")
        .setCheck(null);
    this.appendDummyInput()
        .setAlign(Blockly.ALIGN_RIGHT)
        .appendField("Repetir?")
        .appendField(new Blockly.FieldDropdown([["Automático", "tmr.ALARM_AUTO"], ["No", "tmr.ALARM_SINGLE"], ["Al llamar", "tmr.ALARM_SEMI"]]), "NAME");
    this.setInputsInline(false);
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(65);
    this.setTooltip('');
    this.setHelpUrl('http://www.example.com/');
  }
};
Blockly.Lua['alarm_create'] = function(block) {
  var variable_name_alarm = Blockly.Lua.nameDB_.getName(block.getFieldValue('name_alarm'), Blockly.Variables.CATEGORY_NAME);
  var value_interval = Blockly.Lua.valueToCode(block, 'interval', Blockly.Lua.ORDER_ATOMIC);
  var statements_name = Blockly.Lua.statementToCode(block, 'NAME');
  var dropdown_name = block.getFieldValue('NAME');
  // TODO: Assemble Lua into code variable.
  var code = variable_name_alarm + ' = tmr.create()\n' +
		variable_name_alarm + ':register( '+ value_interval +', '+dropdown_name+', function()\n'+
		statements_name +
		'end)\n';
  return code;
}
// END TMR ALARM CREATE

// BEGIN TMR ALARM START
Blockly.Blocks['alarm_start'] = {
  init: function() {
    this.appendDummyInput()
        .setAlign(Blockly.ALIGN_RIGHT)
        .appendField(new Blockly.FieldImage("dependencies/modulesNodeMCU/javascript/imag/timer.gif", 30, 30, "*"))
        .appendField("INICIAR")
        .appendField(new Blockly.FieldVariable("mi_alarma"), "name_alarm");
    this.setInputsInline(false);
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(65);
    this.setTooltip('');
    this.setHelpUrl('http://www.example.com/');
  }
};
Blockly.Lua['alarm_start'] = function(block) {
  var variable_name_alarm = Blockly.Lua.nameDB_.getName(block.getFieldValue('name_alarm'), Blockly.Variables.CATEGORY_NAME);
  // TODO: Assemble Lua into code variable.
  var code = variable_name_alarm + ':start()\n';
  return code;
};
// END TMR ALARM START

// BEGIN TMR ALARM STOP
Blockly.Blocks['alarm_stop'] = {
  init: function() {
    this.appendDummyInput()
        .setAlign(Blockly.ALIGN_RIGHT)
        .appendField(new Blockly.FieldImage("dependencies/modulesNodeMCU/javascript/imag/timer.gif", 30, 30, "*"))
        .appendField("DETENER")
        .appendField(new Blockly.FieldVariable("mi_alarma"), "name_alarm");
    this.setInputsInline(false);
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(65);
    this.setTooltip('');
    this.setHelpUrl('http://www.example.com/');
  }
};
Blockly.Lua['alarm_stop'] = function(block) {
  var variable_name_alarm = Blockly.Lua.nameDB_.getName(block.getFieldValue('name_alarm'), Blockly.Variables.CATEGORY_NAME);
  // TODO: Assemble Lua into code variable.
  var code = variable_name_alarm + ':stop()\n';
  return code;
};
// END TMR ALARM STOP
