# Compilación de modulesNodeMCU

El script **make_pack.sh** es responsable de compilar las librerías creadas para luabotv2;
éstas librerías son las quedarán en el toolbox de la aplicación para que el usuario
pueda desarrollar sus proyectos.

```bash
./make_pack.sh mobile luna8266 # para versión movil
```

```bash
./make_pack.sh desktop luna8266 # para versión desktop
```

Blockly.Variables.NAME_TYPE
Blockly.Variables.CATEGORY_NAME
```vim
:%s/NAME_TYPE/CATEGORY_NAME/g
```
Johnny Cubides
