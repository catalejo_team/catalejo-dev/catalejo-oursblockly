/*
 *  read any dht sensor
 */
Blockly.Blocks['dht_read'] = {
  init: function() {
    this.appendValueInput("pin")
        .setCheck("Number")
        .appendField(new Blockly.FieldImage("dependencies/modulesNodeMCU/javascript/imag/dht.gif", 20, 15, "*"))
        .appendField("leer DHT en Pin");
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(180);
    this.setTooltip('');
    this.setHelpUrl('http://www.example.com/');
  }
};
Blockly.Lua['dht_read'] = function(block) {
  var value_pin = Blockly.Lua.valueToCode(block, 'pin', Blockly.Lua.ORDER_ATOMIC);
  // TODO: Assemble Lua into code variable.
  var code = 'dhtStatus, dhtTemp, dhtHum, dhtTemp_dec, dhtHum_dec = dht.read11('+value_pin+')\n';
  return code;
};

/*
 * dht view variables
 */
Blockly.Blocks['dht_view'] = {
  init: function() {
    this.appendDummyInput()
        .appendField(new Blockly.FieldImage("dependencies/modulesNodeMCU/javascript/imag/dht.gif", 20, 15, "*"))
        .appendField("DHT.")
        .appendField(new Blockly.FieldDropdown([["Temperatura", "dhtTemp"], ["Humedad", "dhtHum"], ["Estado sensor", "dhtStatus"], ["Temperatura decimal", "dhtTemp_dec"], ["Humedad decimal", "dhtHum_dec"]]), "out");
    this.setOutput(true, "Number");
    this.setColour(180);
    this.setTooltip('');
    this.setHelpUrl('http://www.example.com/');
  }
};

Blockly.Lua['dht_view'] = function(block) {
  var dropdown_out = block.getFieldValue('out');
  // TODO: Assemble Lua into code variable.
  var code = dropdown_out;
  // TODO: Change ORDER_NONE to the correct strength.
  return [code, Blockly.Lua.ORDER_NONE];
};

//  ULTRASONIC HCSR04 START
//
Blockly.Blocks['hcsr04_lib'] = {
  init: function() {
    this.appendDummyInput()
        .appendField(new Blockly.FieldImage("dependencies/modulesNodeMCU/javascript/imag/ultra.gif", 30, 15, { alt: "*", flipRtl: "FALSE" }))
        .appendField("Lib ultrasonido");
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(180);
 this.setTooltip("La librería ultrasonido permite el uso de sensores de ultrasonido del tipo HCSR04. Debe quedar por encima de los bloques de creación de ultrasonido");
 this.setHelpUrl("");
  }
};

Blockly.Lua['hcsr04_lib'] = function(block) {
  // TODO: Assemble Lua into code variable.
  var code = `dofile("hcsr04.lc")
`;
  return code;
};

Blockly.Blocks['ultrasonic_setup'] = {
  init: function() {
    this.appendDummyInput()
        .setAlign(Blockly.ALIGN_CENTRE)
        .appendField("Crear");
    this.appendDummyInput()
        .setAlign(Blockly.ALIGN_CENTRE)
        .appendField(new Blockly.FieldImage("dependencies/modulesNodeMCU/javascript/imag/ultra.gif", 30, 15, { alt: "*", flipRtl: "FALSE" }))
        .appendField(new Blockly.FieldVariable("item"), "NAME");
    this.appendValueInput("trigger pin")
        .setCheck("Number")
        .setAlign(Blockly.ALIGN_RIGHT)
        .appendField("Trig");
    this.appendValueInput("echo pin")
        .setCheck("Number")
        .setAlign(Blockly.ALIGN_RIGHT)
        .appendField("echo");
    this.setInputsInline(true);
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(180);
 this.setTooltip("Bloque permite crear un sensor ultrasonido hcsr04 a través de sus pines echo y trigger, se puede seleccionar cualquier pin entre D1 a D10.");
 this.setHelpUrl("");
  }
};

Blockly.Lua['ultrasonic_setup'] = function(block) {
  var variable_name = Blockly.Lua.nameDB_.getName(block.getFieldValue('NAME'), Blockly.Variables.CATEGORY_NAME);
  var value_trigger_pin = Blockly.Lua.valueToCode(block, 'trigger pin', Blockly.Lua.ORDER_ATOMIC);
  var value_echo_pin = Blockly.Lua.valueToCode(block, 'echo pin', Blockly.Lua.ORDER_ATOMIC);
  // TODO: Assemble Lua into code variable.
  var code = variable_name + ' = HCSR04.new(' + value_trigger_pin + ', ' + value_echo_pin + ')\n';
  return code;
};

Blockly.Blocks['ultrasonic_set'] = {
  init: function() {
    this.appendDummyInput()
        .appendField(new Blockly.FieldImage("dependencies/modulesNodeMCU/javascript/imag/ultra.gif", 30, 15, { alt: "*", flipRtl: "FALSE" }))
        .appendField(new Blockly.FieldVariable("item"), "ultrasonic")
        .appendField(new Blockly.FieldDropdown([["Emitir ultrasonido",".trigger()"], ["Activar sensor",".activate()"], ["Desactivar sensor",".deactivate()"]]), "option");
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(180);
 this.setTooltip("Permite configurar el sensor dependiendo la opción seleccionada: Si selecciona la opción \"Emitir ultrasonido\" se da la orden de enviar una onda de sonido y preparar al sensor para detectar el eco de la misma. Las opciones otras opciones activan o desactivan el sensor de ultrasonido.");
 this.setHelpUrl("");
  }
};

Blockly.Lua['ultrasonic_set'] = function(block) {
  var variable_ultrasonic = Blockly.Lua.nameDB_.getName(block.getFieldValue('ultrasonic'), Blockly.Variables.CATEGORY_NAME);
  var dropdown_option = block.getFieldValue('option');
  // TODO: Assemble Lua into code variable.
  var code = variable_ultrasonic + dropdown_option + "\n";
  return code;
};

Blockly.Blocks['ultrasonic_get'] = {
  init: function() {
    this.appendDummyInput()
        .appendField(new Blockly.FieldImage("dependencies/modulesNodeMCU/javascript/imag/ultra.gif", 30, 15, { alt: "*", flipRtl: "FALSE" }))
        .appendField(new Blockly.FieldVariable("item"), "ultrasonic")
        .appendField(new Blockly.FieldDropdown([["Obtener distancia en cm",".measure()/10"], ["Leer el estado del sensor",".status()"]]), "option");
    this.setOutput(true, null);
    this.setColour(180);
 this.setTooltip("Si se selecciona la opción \"Leer distancia en cm\" se entrega el valor de la distancia obtenida de un objeto con el sensor, esta distancia se reporta en cm. Es importante reconocer que una lectura del sensor válida requiere monitorear el estado del sensor el cual se obtiene a través de la opción \"Leer el estado del sensor\", el estado reportado tiene las siguientes opciones; -2: sensor activo, -1: Error en la lectura, 0: Distancia obtenida, 1: Lectura pendiente, 2: sensor desactivado.");
 this.setHelpUrl("");
  }
};

Blockly.Lua['ultrasonic_get'] = function(block) {
  var variable_ultrasonic = Blockly.Lua.nameDB_.getName(block.getFieldValue('ultrasonic'), Blockly.Variables.CATEGORY_NAME);
  var dropdown_option = block.getFieldValue('option');
  // TODO: Assemble Lua into code variable.
  var code = variable_ultrasonic + dropdown_option;
  // TODO: Change ORDER_NONE to the correct strength.
  return [code, Blockly.Lua.ORDER_NONE];
};

Blockly.Blocks['ultrasonic_status'] = {
  init: function() {
    this.appendDummyInput()
        .appendField(new Blockly.FieldImage("dependencies/modulesNodeMCU/javascript/imag/ultra.gif", 30, 15, { alt: "*", flipRtl: "FALSE" }))
        .appendField(new Blockly.FieldDropdown([["Tarea completada","0"], ["Lectura en proceso","1"], ["Error en la lectura","-1"], ["Sensor activado","-2"], ["Sensor desactivado","2"]]), "status");
    this.setOutput(true, null);
    this.setColour(180);
 this.setTooltip("Es importante reconocer que una lectura del sensor válida requiere monitorear el estado del sensor, el estado reportado tiene las siguientes opciones; -2: sensor activo, -1: Error en la lectura, 0: Distancia obtenida, 1: Lectura pendiente, 2: sensor desactivado.");
 this.setHelpUrl("");
  }
};

Blockly.Lua['ultrasonic_status'] = function(block) {
  var dropdown_status = block.getFieldValue('status');
  // TODO: Assemble Lua into code variable.
  var code = dropdown_status;
  // TODO: Change ORDER_NONE to the correct strength.
  return [code, Blockly.Lua.ORDER_NONE];
};

Blockly.Blocks['ultrasonic_if_status'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("Si el sensor")
        .appendField(new Blockly.FieldImage("dependencies/modulesNodeMCU/javascript/imag/ultra.gif", 30, 15, { alt: "*", flipRtl: "FALSE" }))
        .appendField(new Blockly.FieldVariable("item"), "NAME");
    this.appendDummyInput()
        .appendField("tiene el estado:")
        .appendField(new Blockly.FieldDropdown([["Tarea completada","0"], ["Lectura en proceso","1"], ["Error en la lectura","-1"], ["Sensor activado","-2"], ["Sensor desactivado","2"]]), "status");
    this.appendStatementInput("do it")
        .setCheck(null)
        .appendField("Hacer");
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(180);
 this.setTooltip("Este bloque permite realizar una tarea si el sensor ultrasonido se encuentra en el estado seleccionado. En \"item\" poner la variable del sensor iniciado, en estado seleccionar la opción que quiere comparar, en \"hacer\" poner la acción que quiere se realice si se cumple la condición a observar en el estado.");
 this.setHelpUrl("");
  }
};

Blockly.Lua['ultrasonic_if_status'] = function(block) {
  var variable_name = Blockly.Lua.nameDB_.getName(block.getFieldValue('NAME'), Blockly.Variables.CATEGORY_NAME);
  var dropdown_status = block.getFieldValue('status');
  var statements_do_it = Blockly.Lua.statementToCode(block, 'do it');
  // TODO: Assemble Lua into code variable.
  var code = "if " + variable_name + ".status() == " + dropdown_status + " then\n" +
        statements_do_it +
        "end\n";
  return code;
};

Blockly.Blocks['ultrasonic_if_status_ok'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("Si lectura en")
        .appendField(new Blockly.FieldImage("dependencies/modulesNodeMCU/javascript/imag/ultra.gif", 30, 15, { alt: "*", flipRtl: "FALSE" }))
        .appendField(new Blockly.FieldVariable("item"), "NAME");
    this.appendDummyInput()
        .appendField("es completada con éxito");
    this.appendStatementInput("do it")
        .setCheck(null)
        .appendField("Hacer");
    this.appendDummyInput()
        .appendField("Solicitar una nueva lectura")
        .appendField(new Blockly.FieldCheckbox("TRUE"), "trigger");
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(180);
 this.setTooltip("Este bloque permite realizar una tarea cuando la lectura del sensor es completada satisfactoriamente. En \"item\" poner la variable del sensor iniciado, en \"hacer\" poner la acción que quiere se realice si se cumple la condición a observar en el estado, si quiere lecturas continuas del sensor active el check para realizar un nuevo trigger.");
 this.setHelpUrl("");
  }
};

Blockly.Lua['ultrasonic_if_status_ok'] = function(block) {
  var variable_name = Blockly.Lua.nameDB_.getName(block.getFieldValue('NAME'), Blockly.Variables.CATEGORY_NAME);
  var statements_do_it = Blockly.Lua.statementToCode(block, 'do it');
  var checkbox_trigger = block.getFieldValue('trigger') === 'TRUE';
  // TODO: Assemble Lua into code variable.
  var activate_trigger = "";
  if( checkbox_trigger == true) {
    activate_trigger = "if " + variable_name + ".status() < 1 then\n\t" + variable_name + ".trigger()\nend\n"
  }
  var code = "if " + variable_name + ".status() == 0 then\n" +
        statements_do_it +
        "end\n" + activate_trigger;
  return code;
};

// TODO: NO REMOVER HASTA NO REVISAR LIBRERÍA ANTIGUA
// Blockly.Blocks['hcsr04_declare'] = {
//   init: function() {
//     this.appendDummyInput()
//         .setAlign(Blockly.ALIGN_CENTRE)
//         .appendField("Asignar");
//     this.appendDummyInput()
//         .setAlign(Blockly.ALIGN_CENTRE)
//         .appendField(new Blockly.FieldImage("dependencies/modulesNodeMCU/javascript/imag/ultra.gif", 50, 30, "*"))
//         .appendField(new Blockly.FieldVariable("item"), "NAME");
//     this.appendValueInput("trig")
//         .setCheck("Number")
//         .setAlign(Blockly.ALIGN_RIGHT)
//         .appendField("Disparo pin");
//     this.appendValueInput("echo")
//         .setCheck("Number")
//         .setAlign(Blockly.ALIGN_RIGHT)
//         .appendField("Echo pin");
//     this.appendValueInput("max_distance")
//         .setCheck("Number")
//         .setAlign(Blockly.ALIGN_RIGHT)
//         .appendField("Distancia max");
//     this.appendValueInput("avg_readings")
//         .setCheck("Number")
//         .setAlign(Blockly.ALIGN_RIGHT)
//         .appendField("Muestras");
//     this.setPreviousStatement(true, null);
//     this.setNextStatement(true, null);
//     this.setColour(180);
//     this.setTooltip('');
//     this.setHelpUrl('http://www.example.com/');
//   }
// };

// Blockly.Lua['hcsr04_declare'] = function(block) {
//   var variable_name = Blockly.Lua.nameDB_.getName(block.getFieldValue('NAME'), Blockly.Variables.CATEGORY_NAME);
//   var value_trig = Blockly.Lua.valueToCode(block, 'trig', Blockly.Lua.ORDER_ATOMIC);
//   var value_echo = Blockly.Lua.valueToCode(block, 'echo', Blockly.Lua.ORDER_ATOMIC);
//   var value_max_distance = Blockly.Lua.valueToCode(block, 'max_distance', Blockly.Lua.ORDER_ATOMIC);
//   var value_avg_readings = Blockly.Lua.valueToCode(block, 'avg_readings', Blockly.Lua.ORDER_ATOMIC);
//   // TODO: Assemble Lua into code variable.
//   var code = `${variable_name} = HCSR04(${value_trig}, ${value_echo}, ${value_max_distance}, ${value_avg_readings})
// `
//   return code;
// };

// Blockly.Blocks['hcsr04_read'] = {
//   init: function() {
//     this.appendDummyInput()
//         .setAlign(Blockly.ALIGN_CENTRE)
//         .appendField("Leer distancia");
//     this.appendDummyInput()
//         .setAlign(Blockly.ALIGN_CENTRE)
//         .appendField(new Blockly.FieldImage("dependencies/modulesNodeMCU/javascript/imag/ultra.gif", 30, 15, "*"))
//         .appendField(new Blockly.FieldVariable("item"), "sensor");
//     this.setPreviousStatement(true, null);
//     this.setNextStatement(true, null);
//     this.setColour(180);
//     this.setTooltip('');
//     this.setHelpUrl('http://www.example.com/');
//   }
// };

// Blockly.Lua['hcsr04_read'] = function(block) {
//   var variable_sensor = Blockly.Lua.nameDB_.getName(block.getFieldValue('sensor'), Blockly.Variables.CATEGORY_NAME);
//   // TODO: Assemble Lua into code variable.
//   // var code = '...\n';
//   var code = `${variable_sensor}.measure()
// `;
//   return code;
// };

// Blockly.Blocks['hcsr04_distance'] = {
//   init: function() {
//     this.appendDummyInput()
//         .appendField("De")
//         .appendField(new Blockly.FieldImage("dependencies/modulesNodeMCU/javascript/imag/ultra.gif", 30, 15, "*"))
//         .appendField(new Blockly.FieldVariable("item"), "sensor")
//         .appendField("obtener")
//     this.appendDummyInput()
//         .appendField("distancia en")
//         .appendField(new Blockly.FieldDropdown([["centímetros", "distance * 100"], ["metros", "distance"]]), "escala");
//     this.setOutput(true, "Number");
//     this.setColour(180);
//     this.setTooltip('');
//     this.setHelpUrl('http://www.example.com/');
//   }
// };

// Blockly.Lua['hcsr04_distance'] = function(block) {
//   var variable_sensor = Blockly.Lua.nameDB_.getName(block.getFieldValue('sensor'), Blockly.Variables.CATEGORY_NAME);
//   var dropdown_escala = block.getFieldValue('escala');
//   // TODO: Assemble Lua into code variable.
//   var code = `${variable_sensor}.${dropdown_escala}`;
//   // TODO: Change ORDER_NONE to the correct strength.
//   return [code, Blockly.Lua.ORDER_NONE];
// };

/*
 * hcsr04_read
 */
 // Blockly.Blocks['hcsr04_read'] = {
 //  init: function() {
 //    this.appendDummyInput()
 //        .setAlign(Blockly.ALIGN_CENTRE)
 //        .appendField(new Blockly.FieldImage("dependencies/modulesNodeMCU/javascript/imag/ultra.gif", 30, 15, "*"))
 //        .appendField("Leer distancia en")
 //        .appendField(new Blockly.FieldVariable("item"), "NAME");
 //    this.setInputsInline(false);
 //    this.setOutput(true, null);
 //    this.setColour(300);
 //    this.setTooltip('');
 //    this.setHelpUrl('http://www.example.com/');
 //  }
// };

// Blockly.Lua['hcsr04_read'] = function(block) {
 //  var variable_name = Blockly.Lua.nameDB_.getName(block.getFieldValue('NAME'), Blockly.Variables.CATEGORY_NAME);
 //  // TODO: Assemble Lua into code variable.
 //  var code = variable_name+'.measure()';
 //  // TODO: Change ORDER_NONE to the correct strength.
 //  return [code, Blockly.Lua.ORDER_NONE];
// };

/*
 * DS18B20 START
 * */
Blockly.Blocks['ds18b20_lib'] = {
  init: function() {
    this.appendDummyInput()
        .setAlign(Blockly.ALIGN_CENTRE)
        .appendField(new Blockly.FieldImage("dependencies/modulesNodeMCU/javascript/imag/termometro.png", 25, 25, "*"))
        .appendField("Iniciar DS18B20");
    this.appendDummyInput()
        .setAlign(Blockly.ALIGN_CENTRE)
        .appendField("y guardar temperatura");
    this.appendDummyInput()
        .setAlign(Blockly.ALIGN_CENTRE)
        .appendField("En: ")
        .appendField(new Blockly.FieldVariable("item"), "temp");
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(180);
    this.setTooltip('');
    this.setHelpUrl('http://www.example.com/');
  }
};

Blockly.Lua['ds18b20_lib'] = function(block) {
  var variable_temp = Blockly.Lua.nameDB_.getName(block.getFieldValue('temp'), Blockly.Variables.CATEGORY_NAME);
  // TODO: Assemble Lua into code variable.
  var code = `
file.remove("ds18b20_save.lc")
_ds18b20 = require("ds18b20")
function readout(temps_)
  i_ = 0
  for i, s in ipairs(ds18b20.sens) do
    i_ = i
  end
  if i_ == 1 then
    for _addr, _temp in pairs(temps_) do
      ${variable_temp} = _temp
    end
  else
    ${variable_temp} = -255
  end
end
`;
  return code;
};

/*
 * DS18B20 READ
 * */

Blockly.Blocks['ds18b20_read'] = {
  init: function() {
    this.appendDummyInput()
        .setAlign(Blockly.ALIGN_CENTRE)
        .appendField(new Blockly.FieldImage("dependencies/modulesNodeMCU/javascript/imag/termometro.png", 25, 25, "*"))
        .appendField("Leer DS18B20");
    this.appendValueInput("Pin")
        .setCheck("Number")
        .setAlign(Blockly.ALIGN_CENTRE)
        .appendField("En el Pin");
    this.appendDummyInput()
        .setAlign(Blockly.ALIGN_RIGHT)
        .appendField("en la escala")
        .appendField(new Blockly.FieldDropdown([["Celsius", "ds18b20.C"], ["Kelvin", "ds18b20.K"], ["Fahrenheit", "ds18b20.F"]]), "scale");
    this.setInputsInline(false);
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(180);
    this.setTooltip('');
    this.setHelpUrl('http://www.example.com/');
  }
};

Blockly.Lua['ds18b20_read'] = function(block) {
  var value_pin = Blockly.Lua.valueToCode(block, 'Pin', Blockly.Lua.ORDER_ATOMIC);
  var dropdown_scale = block.getFieldValue('scale');
  // TODO: Assemble Lua into code variable.
  var code = `_ds18b20:read_temp(readout, ${value_pin}, ${dropdown_scale})
_ds18b20:read_temp(readout, ${value_pin})
`;
  return code;
};

