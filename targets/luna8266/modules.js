// BEGIN PWM SETUP
Blockly.Blocks['pwm_setup'] = {
  init: function() {
    this.appendDummyInput()
        .setAlign(Blockly.ALIGN_CENTRE)
        .appendField(new Blockly.FieldImage("dependencies/modulesNodeMCU/javascript/imag/PWM.gif", 30, 30, "*"));
    this.appendValueInput("pin")
        .setCheck(null)
        .setAlign(Blockly.ALIGN_RIGHT)
        .appendField("Pin:");
    this.appendValueInput("frec")
        .setCheck(null)
        .setAlign(Blockly.ALIGN_RIGHT)
        .appendField("Frecuencia:");
    this.appendValueInput("duty")
        .setCheck(null)
        .setAlign(Blockly.ALIGN_RIGHT)
        .appendField("Ciclo útil:");
    this.setInputsInline(false);
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(290);
    this.setTooltip('');
    this.setHelpUrl('http://www.example.com/');
  }
};
Blockly.Lua['pwm_setup'] = function(block) {
  var value_pin = Blockly.Lua.valueToCode(block, 'pin', Blockly.Lua.ORDER_ATOMIC);
  var value_frec = Blockly.Lua.valueToCode(block, 'frec', Blockly.Lua.ORDER_ATOMIC);
  var value_duty = Blockly.Lua.valueToCode(block, 'duty', Blockly.Lua.ORDER_ATOMIC);
  // TODO: Assemble Lua into code variable.
  var code = 'pwm.setup('+value_pin+', '+value_frec+', '+value_duty+')\n';
  return code;
};
// END PWM SETUP

// BEGIN PWM START
Blockly.Blocks['pwm_start'] = {
  init: function() {
    this.appendDummyInput()
        .setAlign(Blockly.ALIGN_CENTRE)
        .appendField("INICIAR");
    this.appendValueInput("pin")
        .setCheck("Number")
        .setAlign(Blockly.ALIGN_CENTRE)
        .appendField(new Blockly.FieldImage("dependencies/modulesNodeMCU/javascript/imag/PWM.gif", 30, 30, "*"))
        .appendField("En pin:");
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(290);
    this.setTooltip('');
    this.setHelpUrl('http://www.example.com/');
  }
};
Blockly.Lua['pwm_start'] = function(block) {
  var value_pin = Blockly.Lua.valueToCode(block, 'pin', Blockly.Lua.ORDER_ATOMIC);
  // TODO: Assemble Lua into code variable.
  var code = 'pwm.start('+value_pin+')\n';
  return code;
};
// END PWM START

// BEGIN PWM STOP

Blockly.Blocks['pwm_stop'] = {
  init: function() {
    this.appendDummyInput()
        .setAlign(Blockly.ALIGN_CENTRE)
        .appendField("DETENER");
    this.appendValueInput("pin")
        .setCheck("Number")
        .setAlign(Blockly.ALIGN_CENTRE)
        .appendField(new Blockly.FieldImage("dependencies/modulesNodeMCU/javascript/imag/PWM.gif", 30, 30, "*"))
        .appendField("En pin:");
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(290);
    this.setTooltip('');
    this.setHelpUrl('http://www.example.com/');
  }
};
Blockly.Lua['pwm_stop'] = function(block) {
  var value_pin = Blockly.Lua.valueToCode(block, 'pin', Blockly.Lua.ORDER_ATOMIC);
  // TODO: Assemble Lua into code variable.
  var code = 'pwm.stop('+value_pin+')\n';
  return code;
};
// END PWM STOP

// BEGIN PWM CHANGE FRECUENCY
Blockly.Blocks['pwm_change_frecuency'] = {
  init: function() {
    this.appendDummyInput()
        .setAlign(Blockly.ALIGN_CENTRE)
        .appendField(new Blockly.FieldImage("dependencies/modulesNodeMCU/javascript/imag/PWM.gif", 30, 30, "*"));
    this.appendValueInput("pin")
        .setCheck(null)
        .setAlign(Blockly.ALIGN_RIGHT)
        .appendField("En pin:");
    this.appendDummyInput()
        .setAlign(Blockly.ALIGN_CENTRE)
        .appendField("CAMBIAR");
    this.appendValueInput("frec")
        .setCheck(null)
        .setAlign(Blockly.ALIGN_RIGHT)
        .appendField("Frecuencia:");
    this.setInputsInline(false);
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(290);
    this.setTooltip('');
    this.setHelpUrl('http://www.example.com/');
  }
};
Blockly.Lua['pwm_change_frecuency'] = function(block) {
  var value_pin = Blockly.Lua.valueToCode(block, 'pin', Blockly.Lua.ORDER_ATOMIC);
  var value_frec = Blockly.Lua.valueToCode(block, 'frec', Blockly.Lua.ORDER_ATOMIC);
  // TODO: Assemble Lua into code variable.
  var code = 'pwm.setclock('+value_pin+', '+value_frec+')\n';
  return code;
};
// END PWM CHANGE FRECUENCY

// BEGIN PWM CHANGE DUTY
Blockly.Blocks['pwm_change_duty'] = {
  init: function() {
    this.appendDummyInput()
        .setAlign(Blockly.ALIGN_CENTRE)
        .appendField(new Blockly.FieldImage("dependencies/modulesNodeMCU/javascript/imag/PWM.gif", 30, 30, "*"));
    this.appendValueInput("pin")
        .setCheck(null)
        .setAlign(Blockly.ALIGN_RIGHT)
        .appendField("En pin:");
    this.appendDummyInput()
        .setAlign(Blockly.ALIGN_CENTRE)
        .appendField("CAMBIAR");
    this.appendValueInput("duty")
        .setCheck(null)
        .setAlign(Blockly.ALIGN_RIGHT)
        .appendField("Ciclo útil:");
    this.setInputsInline(false);
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(290);
    this.setTooltip('');
    this.setHelpUrl('http://www.example.com/');
  }
};
Blockly.Lua['pwm_change_duty'] = function(block) {
  var value_pin = Blockly.Lua.valueToCode(block, 'pin', Blockly.Lua.ORDER_ATOMIC);
  var value_duty = Blockly.Lua.valueToCode(block, 'duty', Blockly.Lua.ORDER_ATOMIC);
  // TODO: Assemble Lua into code variable.
  var code = 'pwm.setduty('+value_pin+', '+value_duty+')\n';
  return code;
};
// END PWM CHANGE DUTY
