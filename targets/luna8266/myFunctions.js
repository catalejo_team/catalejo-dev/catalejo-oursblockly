/** CATELEJO MAP() **/

Blockly.Blocks['function_map'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("cambiar rango");
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(285);
    this.setTooltip('');
    this.setHelpUrl('http://www.example.com/');
  }
};

Blockly.Lua['function_map'] = function(block) {
  // TODO: Assemble Lua into code variable.
  var code = `function catalejo_map(x, x1, y1, x2, y2)
  return (y2-y1)/(x2-x1)*(x - x1) + y1
end
`;
  return code;
};

/** CATELEJO MAP() RETURN **/

Blockly.Blocks['function_map_return'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("cambiar rango");
    this.appendValueInput("x")
        .setCheck("Number")
.setAlign(Blockly.ALIGN_RIGHT)
        .appendField("variable:");
    this.appendValueInput("x1")
        .setCheck("Number")
.setAlign(Blockly.ALIGN_RIGHT)
        .appendField("x1:");
    this.appendValueInput("y1")
        .setCheck("Number")
.setAlign(Blockly.ALIGN_RIGHT)
        .appendField("y1:");
    this.appendValueInput("x2")
        .setCheck("Number")
.setAlign(Blockly.ALIGN_RIGHT)
        .appendField("x2:");
    this.appendValueInput("y2")
        .setCheck("Number")
.setAlign(Blockly.ALIGN_RIGHT)
        .appendField("y2:");
    this.setOutput(true, "Number");
    this.setColour(285);
    this.setInputsInline(false);
    this.setTooltip('');
    this.setHelpUrl('http://www.example.com/');
  }
};

Blockly.Lua['function_map_return'] = function(block) {
  var value_x = Blockly.Lua.valueToCode(block, 'x', Blockly.Lua.ORDER_ATOMIC);
  var value_x1 = Blockly.Lua.valueToCode(block, 'x1', Blockly.Lua.ORDER_ATOMIC);
  var value_y1 = Blockly.Lua.valueToCode(block, 'y1', Blockly.Lua.ORDER_ATOMIC);
  var value_x2 = Blockly.Lua.valueToCode(block, 'x2', Blockly.Lua.ORDER_ATOMIC);
  var value_y2 = Blockly.Lua.valueToCode(block, 'y2', Blockly.Lua.ORDER_ATOMIC);
  // TODO: Assemble Lua into code variable.
  var code = `catalejo_map(${value_x}, ${value_x1}, ${value_y1}, ${value_x2}, ${value_y2})`;
  // TODO: Change ORDER_NONE to the correct strength.
  return [code, Blockly.Lua.ORDER_NONE];
};

Blockly.Blocks['lib_change_range'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("Crear")
        .appendField(new Blockly.FieldVariable("map"), "map")
        .appendField("y=mx+b,");
    this.appendValueInput("x1")
        .setCheck("Number")
        .appendField("x1");
    this.appendValueInput("y1")
        .setCheck("Number")
        .appendField("y1");
    this.appendValueInput("x2")
        .setCheck("Number")
        .appendField("x2");
    this.appendValueInput("y2")
        .setCheck("Number")
        .appendField("y2");
    this.setInputsInline(true);
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(290);
 this.setTooltip("Crear una función de cambio de rango, a la ecuación lineal se debe agregar los puntos (y2, x2) y (y1,x1) para poder calcular el cambio de rango.");
 this.setHelpUrl("");
  }
};

Blockly.Lua['lib_change_range'] = function(block) {
  var variable_map = Blockly.Lua.nameDB_.getName(block.getFieldValue('map'), Blockly.Variables.CATEGORY_NAME);
  var value_x1 = Blockly.Lua.valueToCode(block, 'x1', Blockly.Lua.ORDER_ATOMIC);
  var value_y1 = Blockly.Lua.valueToCode(block, 'y1', Blockly.Lua.ORDER_ATOMIC);
  var value_x2 = Blockly.Lua.valueToCode(block, 'x2', Blockly.Lua.ORDER_ATOMIC);
  var value_y2 = Blockly.Lua.valueToCode(block, 'y2', Blockly.Lua.ORDER_ATOMIC);
  // TODO: Assemble Lua into code variable.
  var m = ((Number(value_y2) - Number(value_y1)) / (Number(value_x2) - Number(value_x1))).toFixed(3);
  var b = (-1*(m*Number(value_x1)) + Number(value_y1)).toFixed(3);
  var code = `${variable_map} = function (x)
  return ${m}*x + ${b}
end
`;
  return code;
};

Blockly.Blocks['return_change_range'] = {
  init: function() {
    this.appendValueInput("x")
        .setCheck("Number")
        .appendField("Calcular")
        .appendField(new Blockly.FieldVariable("map"), "map")
        .appendField("(x)");
    this.setInputsInline(false);
    this.setOutput(true, "Number");
    this.setColour(290);
 this.setTooltip("Calcular el resultado de la función cambio de rango configurada con el nombre de la variable.");
 this.setHelpUrl("");
  }
};

Blockly.Lua['return_change_range'] = function(block) {
  var variable_map = Blockly.Lua.nameDB_.getName(block.getFieldValue('map'), Blockly.Variables.CATEGORY_NAME);
  var value_x = Blockly.Lua.valueToCode(block, 'x', Blockly.Lua.ORDER_ATOMIC);
  // TODO: Assemble Lua into code variable.
  var code = variable_map + '('+ value_x +')';
  // TODO: Change ORDER_NONE to the correct strength.
  return [code, Blockly.Lua.ORDER_NONE];
};
