// BEGIN HTML
Blockly.Blocks['html_page'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("HTML")
        .appendField("Título:")
        .appendField(new Blockly.FieldTextInput("Web"), "title");
    this.appendStatementInput("view")
        .setCheck(null)
        .appendField("Vista");
    this.appendStatementInput("logic")
        .setCheck(null)
        .appendField("Lógica");
	this.setPreviousStatement(true, null);
	this.setNextStatement(true, null);
    this.setColour(255);
    this.setTooltip('');
    this.setHelpUrl('http://www.example.com/');
  }
};

Blockly.Lua['html_page'] = function(block) {
  var text_title = block.getFieldValue('title');
  var statements_view = Blockly.Lua.statementToCode(block, 'view');
  var statements_logic = Blockly.Lua.statementToCode(block, 'logic');
  // TODO: Assemble Lua into code variable.
  var code = 'srv=net.createServer(net.TCP)\n' +
	'srv:listen(80,function(conn)\n' +
	'\tconn:on("receive", function(client,request)\n' +
	'\t\tlocal _, _, method, path, vars = string.find(request, "([A-Z]+) (.+)?(.+) HTTP")\n' +
	'\t\tif(method == nil)then\n' +
	'\t\t\t_, _, method, path = string.find(request, "([A-Z]+) (.+) HTTP")\n' +
	'\t\tend\n' +
	'\t\tlocal _GET = {}\n' +
	'\t\tif (vars ~= nil)then\n' +
	'\t\t\tfor k, v in string.gmatch(vars, "(%w+)=(%w+)&*") do\n' +
	'\t\t\t\t_GET[k] = v\n' +
	'\t\t\tend\n' +
	'\t\tend\n' +
	'\t\tlocal _on,_off = "",""\n' +
	"\t" + statements_logic +
	'\t\tlocal _html = "<!DOCTYPE html>\\r\\n"\n'+
    '\t\t.."<html>\\r\\n"\n' +
    '\t\t.."<head>\\r\\n"\n' +
    '\t\t.."<meta name=\\"viewport\\" charset=\\"utf-8\\" content=\\"width=device-width, initial-scale=1\\">\\r\\n"\n' +
    '\t\t.."</head>\\r\\n"\n' +
    '\t\t.."<body>\\r\\n"\n' +
	'\t\t.."<h1>' +text_title+ '</h1>\\r\\n"\n' +
	"\t" + statements_view +
    '\t\t.."</body>\\r\\n"\n' +
    '\t\t.."</html>\\r\\n"\n' +
	'\t\tlocal _http = "HTTP/1.1 200 OK\\r\\n"\n'+
    '\t\t.."Content-Length: "..string.len(_html).."\\r\\n"\n' +
	'\t\t.."Content-Type: text/html\\r\\n\\r\\n"\n'+
    '\t\t.._html\n' +
	'\t\tclient:send(_http)\n' +
	//'client:close()\n' +
	'\t\tcollectgarbage()\n' + 
	'\tend)\n' +
	'end)\n';
  return code;
};
// END HTML

// BEGIN HTML BUTTON VIEW
Blockly.Blocks['html_button_view'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("Botón")
        .appendField(new Blockly.FieldDropdown([["0", "0"], ["1", "1"], ["2", "2"], ["3", "3"], ["4", "4"], ["5", "5"], ["6", "6"], ["7", "7"], ["8", "8"], ["9", "9"], ["10", "10"], ["11", "11"], ["12", "12"]]), "number_button")
        .appendField("Nombre:")
        .appendField(new Blockly.FieldTextInput("Pulsador"), "name_button");
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(255);
    this.setTooltip('');
    this.setHelpUrl('http://www.example.com/');
  }
};
Blockly.Lua['html_button_view'] = function(block) {
  var dropdown_number_button = block.getFieldValue('number_button');
  var text_name_button = block.getFieldValue('name_button');
  // TODO: Assemble Lua into code variable.
  var code = '..\"<p><a href=\\\"?pin=' + dropdown_number_button + '\\\"><button>' + 
  text_name_button + '</button></a></p>\\r\\n"\n';
  return code;
};
// END HTML BUTTON VIEW

// BEGIN HTML BUTTON LOGIC
Blockly.Blocks['html_button_logic'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("Botón")
        .appendField(new Blockly.FieldDropdown([["0", "0"], ["1", "1"], ["2", "2"], ["3", "3"], ["4", "4"], ["5", "5"], ["6", "6"], ["7", "7"], ["8", "8"], ["9", "9"], ["10", "10"], ["11", "11"], ["12", "12"]]), "number_button");
    this.setOutput(true, null);
    this.setColour(255);
    this.setTooltip('');
    this.setHelpUrl('http://www.example.com/');
  }
};

Blockly.Lua['html_button_logic'] = function(block) {
  var dropdown_number_button = block.getFieldValue('number_button');
  // TODO: Assemble Lua into code variable.
  var code = '_GET.pin == "' + dropdown_number_button + '"';
  // TODO: Change ORDER_NONE to the correct strength.
  return [code, Blockly.Lua.ORDER_NONE];
};
// END HTML BUTTON LOGIC

// BEGIN HTML TEXT
Blockly.Blocks['html_text'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("Texto Web:")
        .appendField(new Blockly.FieldTextInput("Pagina Web"), "text_html");
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(255);
    this.setTooltip('');
    this.setHelpUrl('http://www.example.com/');
  }
};
Blockly.Lua['html_text'] = function(block) {
  var text_text_html = block.getFieldValue('text_html');
  // TODO: Assemble Lua into code variable.
  var code = '.."<p>' + text_text_html + '</p>\\r\\n"\n';
  return code;
};
// END HTML TEXT

// BEGIN HTML TEXT ADD VARIABLE
Blockly.Blocks['html_text_add_variable'] = {
  init: function() {
    this.appendValueInput("input")
        .setCheck(null)
        .appendField("Texto Web + variable")
        .appendField(new Blockly.FieldTextInput("Nombre de variable"), "name_variable");
    this.setInputsInline(false);
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(255);
    this.setTooltip('');
    this.setHelpUrl('http://www.example.com/');
  }
};
Blockly.Lua['html_text_add_variable'] = function(block) {
  var text_name_variable = block.getFieldValue('name_variable');
  var value_input = Blockly.Lua.valueToCode(block, 'input', Blockly.Lua.ORDER_ATOMIC);
  // TODO: Assemble Lua into code variable.
  var code = '.."<p>' + text_name_variable + '"..' + value_input +'..' + '"</p>\\r\\n"\n';
  return code;
};
// END HTML TEXT ADD VARIABLE

// BEGIN HTML SIZE TEXT ADD VARIABLE
Blockly.Blocks['html_text_size'] = {
  init: function() {
    this.appendValueInput("size")
        .setCheck(null)
        .appendField("Tamaño letra");
    this.setInputsInline(false);
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(255);
    this.setTooltip('');
    this.setHelpUrl('http://www.example.com/');
  }
};

Blockly.Lua['html_text_size'] = function(block) {
  var value_size = Blockly.Lua.valueToCode(block, 'size', Blockly.Lua.ORDER_ATOMIC);
  // TODO: Assemble Lua into code variable.
  var code = '.."<meta name=\\"viewport\\" content=\\"width=device-width, initial-scale='+value_size+'\\">\\r\\n"\n';
  return code;
};
// END HTML SIZE TEXT ADD VARIABLE
