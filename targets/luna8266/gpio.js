/*
 * Pin number
 */
Blockly.Lua['pin_d'] = function(block) {
  var dropdown_number = block.getFieldValue('number');
  // TODO: Assemble Lua into code variable.
  var code = dropdown_number;
  // TODO: Change ORDER_NONE to the correct strength.
  return [code, Blockly.Lua.ORDER_NONE];
};

Blockly.Blocks['pin_d'] = {
  init: function() {
    this.appendDummyInput()
        .appendField(new Blockly.FieldDropdown([["D0","0"], ["D1","1"], ["D2","2"], ["D3","3"], ["D4","4"], ["D5","5"], ["D6","6"], ["D7","7"], ["D8","8"], ["D9","9"], ["D10","10"]]), "number");
    this.setOutput(true, null);
    this.setColour(210);
 this.setTooltip("Este bloque representa un pin en la tarjeta de desarrollo desde D0 hasta D10. Tener presente que las salidas se representan con números enteros, es decir, para D0 es 0 o para D9 es 9, así se representa para los demás pines.");
 this.setHelpUrl("");
  }
};

/*
 * gpio_mode
 */
Blockly.Blocks['gpio_mode'] = {
  init: function() {
    this.appendValueInput("pin")
        .setCheck("Number")
        .appendField("Modo Pin");
    this.appendDummyInput()
        .appendField(new Blockly.FieldDropdown([["Entrada", "gpio.INPUT"], ["Salida", "gpio.OUTPUT"], ["Interrupción", "gpio.INT"]]), "mode");
    this.setInputsInline(true);
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(210);
    this.setTooltip('');
    this.setHelpUrl('http://www.example.com/');
  }
};


Blockly.Lua['gpio_mode'] = function(block) {
  var value_pin = Blockly.Lua.valueToCode(block, 'pin', Blockly.Lua.ORDER_ATOMIC);
  var dropdown_mode = block.getFieldValue('mode');
  // TODO: Assemble Lua into code variable.
  var code = 'gpio.mode('+value_pin+', '+dropdown_mode+')\n';
  return code;
};

// GPIO MODE WITH VAR 
Blockly.Blocks['create_gpio_var'] = {
  init: function() {
    this.appendValueInput("pin")
        .setCheck("Number")
        .appendField("Crear")
        .appendField(new Blockly.FieldVariable("item"), "pin")
        .appendField("como")
        .appendField(new Blockly.FieldDropdown([["Entrada","gpio.INPUT"], ["Salida","gpio.OUTPUT"], ["Interrupción","gpio.INT"]]), "mode")
        .appendField("en el pin");
    this.setInputsInline(false);
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(210);
 this.setTooltip("Se crea un pin de salida o entrada de información y se asocia a un nombre de variable. Los posibles valores para el mode pin son 3: Entrada, Salida o interrupción.");
 this.setHelpUrl("");
  }
};

Blockly.Lua['create_gpio_var'] = function(block) {
  var variable_pin = Blockly.Lua.nameDB_.getName(block.getFieldValue('pin'), Blockly.Variables.CATEGORY_NAME);
  var dropdown_mode = block.getFieldValue('mode');
  var value_pin = Blockly.Lua.valueToCode(block, 'pin', Blockly.Lua.ORDER_ATOMIC);
  // TODO: Assemble Lua into code variable.
  var code = variable_pin + " = " + value_pin + ";\n" +
    'gpio.mode('+variable_pin+', '+dropdown_mode+')\n';
  return code;
};

/*
 * gpio_write
 */
Blockly.Blocks['gpio_write'] = {
  init: function() {
    this.appendValueInput("pin")
        .setCheck("Number")
        .appendField("Escribir Pin");
    this.appendDummyInput()
        .appendField("a")
        .appendField(new Blockly.FieldDropdown([["Alto", "gpio.HIGH"], ["Bajo", "gpio.LOW"], ["1", "1"], ["0", "0"]]), "write");
    this.setInputsInline(true);
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(210);
    this.setTooltip('');
    this.setHelpUrl('http://www.example.com/');
  }
};

Blockly.Lua['gpio_write'] = function(block) {
  var value_pin = Blockly.Lua.valueToCode(block, 'pin', Blockly.Lua.ORDER_ATOMIC);
  var dropdown_write = block.getFieldValue('write');
  // TODO: Assemble Lua into code variable.
  var code = 'gpio.write('+value_pin+', '+dropdown_write+')\n';
  return code;
};

/*
 * gpio_write_variable
 */
Blockly.Blocks['gpio_write_variable'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("Escribir")
        .appendField(new Blockly.FieldVariable("item"), "object")
        .appendField("a")
        .appendField(new Blockly.FieldDropdown([["Alto", "gpio.HIGH"], ["Bajo", "gpio.LOW"], ["1", "1"], ["0", "0"]]), "write");
    this.setInputsInline(true);
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(210);
    this.setTooltip('');
    this.setHelpUrl('http://www.example.com/');
  }
};

Blockly.Lua['gpio_write_variable'] = function(block) {
  var variable_object = Blockly.Lua.nameDB_.getName(block.getFieldValue('object'), Blockly.Variables.CATEGORY_NAME);
  var dropdown_write = block.getFieldValue('write');
  // TODO: Assemble Lua into code variable.
  var code = 'gpio.write('+variable_object+', '+dropdown_write+')\n';
  return code;
};

Blockly.Blocks['gpio_write_value'] = {
  init: function() {
    this.appendValueInput("pin value")
        .setCheck("Number")
        .appendField("Escribir")
        .appendField(new Blockly.FieldVariable("item"), "var_pin")
        .appendField("a");
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(210);
 this.setTooltip("Pone una variable \"item\" que representa un pin a un valor alto o bajo (1 o 0).");
 this.setHelpUrl("");
  }
};

Blockly.Lua['gpio_write_value'] = function(block) {
  var variable_var_pin = Blockly.Lua.nameDB_.getName(block.getFieldValue('var_pin'), Blockly.Variables.CATEGORY_NAME);
  var value_pin_value = Blockly.Lua.valueToCode(block, 'pin value', Blockly.Lua.ORDER_ATOMIC);
  // TODO: Assemble Lua into code variable.
  var code = "gpio.write(" + variable_var_pin + ", " + value_pin_value + ")\n";
  return code;
};

Blockly.Blocks['gpio_write_pin_value'] = {
  init: function() {
    this.appendValueInput("pin name")
        .setCheck("Number")
        .appendField("Escribir Pin");
    this.appendValueInput("pin value")
        .setCheck("Number")
        .appendField("a");
    this.setInputsInline(true);
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(210);
 this.setTooltip("Pone un Pin a un valor alto o bajo (1 o 0).");
 this.setHelpUrl("");
  }
};

Blockly.Lua['gpio_write_pin_value'] = function(block) {
  var value_pin_name = Blockly.Lua.valueToCode(block, 'pin name', Blockly.Lua.ORDER_ATOMIC);
  var value_pin_value = Blockly.Lua.valueToCode(block, 'pin value', Blockly.Lua.ORDER_ATOMIC);
  // TODO: Assemble Lua into code variable.
  var code = "gpio.write(" + value_pin_name + ", " + value_pin_value + ")\n";
  return code;
};

/*
 * gpio_read
 */
Blockly.Blocks['gpio_read'] = {
  init: function() {
    this.appendValueInput("pin")
        .setCheck("Number")
        .appendField("Leer Pin");
    this.setInputsInline(true);
    this.setOutput(true, "Number");
    this.setColour(210);
    this.setTooltip('');
    this.setHelpUrl('http://www.example.com/');
  }
};

Blockly.Lua['gpio_read'] = function(block) {
  var value_pin = Blockly.Lua.valueToCode(block, 'pin', Blockly.Lua.ORDER_ATOMIC);
  // TODO: Assemble Lua into code variable.
  var code = 'gpio.read('+value_pin+')';
  // TODO: Change ORDER_NONE to the correct strength.
  return [code, Blockly.Lua.ORDER_NONE];
};

/*
 * gpio_read_variable
 */
Blockly.Blocks['gpio_read_variable'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("Leer")
        .appendField(new Blockly.FieldVariable("item"), "Object");
    this.setInputsInline(true);
    this.setOutput(true, "Number");
    this.setColour(210);
    this.setTooltip('');
    this.setHelpUrl('http://www.example.com/');
  }
};

Blockly.Lua['gpio_read_variable'] = function(block) {
  var variable_object = Blockly.Lua.nameDB_.getName(block.getFieldValue('Object'), Blockly.Variables.CATEGORY_NAME);
  // TODO: Assemble Lua into code variable.
  var code = 'gpio.read('+variable_object+')';
  // TODO: Change ORDER_NONE to the correct strength.
  return [code, Blockly.Lua.ORDER_NONE];
};

Blockly.Blocks['gpio_trig'] = {
  init: function() {
    this.appendValueInput("pin")
        .setCheck("Number")
        .appendField("En pin");
    this.appendDummyInput()
        .appendField("cuando")
        .appendField(new Blockly.FieldDropdown([["sube", "up"], ["baja", "down"], ["ambos", "both"], ["nivel bajo", "low"], ["nivel alto", "high"]]), "type");
    this.appendStatementInput("callback")
        .setCheck(null);
    this.setInputsInline(true);
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(210);
    this.setTooltip('');
    this.setHelpUrl('http://www.example.com/');
  }
};

Blockly.Lua['gpio_trig'] = function(block) {
  var value_pin = Blockly.Lua.valueToCode(block, 'pin', Blockly.Lua.ORDER_ATOMIC);
  var dropdown_type = block.getFieldValue('type');
  var statements_callback = Blockly.Lua.statementToCode(block, 'callback');
  // TODO: Assemble Lua into code variable.
  var code = `gpio.trig(${value_pin}, "${dropdown_type}", function(level, when)
  ${statements_callback} end)
`;
  return code;
};

// gpio_trig_var
Blockly.Blocks['gpio_trig_var'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("En")
        .appendField(new Blockly.FieldVariable("item"), "pin");
    this.appendDummyInput()
        .appendField("cuando")
        .appendField(new Blockly.FieldDropdown([["sube", "up"], ["baja", "down"], ["ambos", "both"], ["nivel bajo", "low"], ["nivel alto", "high"]]), "type");
    this.appendStatementInput("callback")
        .setCheck(null);
    this.setInputsInline(true);
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(210);
    this.setTooltip('');
    this.setHelpUrl('http://www.example.com/');
  }
};

Blockly.Lua['gpio_trig_var'] = function(block) {
  var variable_pin = Blockly.Lua.nameDB_.getName(block.getFieldValue('pin'), Blockly.Variables.CATEGORY_NAME);
  var dropdown_type = block.getFieldValue('type');
  var statements_callback = Blockly.Lua.statementToCode(block, 'callback');
  // TODO: Assemble Lua into code variable.
  var code = `gpio.trig(${variable_pin}, "${dropdown_type}", function(level, when)
  ${statements_callback} end)
`;
  return code;
};

// trig level
Blockly.Blocks['gpio_trig_level'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("int.nivel");
    this.setInputsInline(true);
    this.setOutput(true, null);
    this.setColour(210);
    this.setTooltip('');
    this.setHelpUrl('http://www.example.com/');
  }
};

Blockly.Lua['gpio_trig_level'] = function(block) {
  // TODO: Assemble Lua into code variable.
  var code = 'level';
  // TODO: Change ORDER_NONE to the correct strength.
  return [code, Blockly.Lua.ORDER_NONE];
};

// trig when
Blockly.Blocks['gpio_trig_when'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("int.cuando");
    this.setInputsInline(true);
    this.setOutput(true, null);
    this.setColour(210);
    this.setTooltip('');
    this.setHelpUrl('http://www.example.com/');
  }
};

Blockly.Lua['gpio_trig_when'] = function(block) {
  // TODO: Assemble Lua into code variable.
  var code = 'when';
  // TODO: Change ORDER_NONE to the correct strength.
  return [code, Blockly.Lua.ORDER_NONE];
};

// comparator trig
Blockly.Blocks['gpio_trig_compare'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("tipo")
        .appendField(new Blockly.FieldDropdown([["alto", "high"], ["bajo", "low"]]), "level");
    this.setInputsInline(true);
    this.setOutput(true, null);
    this.setColour(210);
    this.setTooltip('');
    this.setHelpUrl('http://www.example.com/');
  }
};

Blockly.Lua['gpio_trig_compare'] = function(block) {
  var dropdown_level = block.getFieldValue('level');
  // TODO: Assemble Lua into code variable.
  var code = `"${dropdown_level}"`;
  // TODO: Change ORDER_NONE to the correct strength.
  return [code, Blockly.Lua.ORDER_NONE];
};

/*
 * adc_read
 */
 Blockly.Blocks['adc_read'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("leer ADC");
    this.setOutput(true, null);
    this.setColour(120);
    this.setTooltip('');
    this.setHelpUrl('http://www.example.com/');
  }
};

Blockly.Lua['adc_read'] = function(block) {
  // TODO: Assemble Lua into code variable.
  var code = 'adc.read(0)';
  // TODO: Change ORDER_NONE to the correct strength.
  return [code, Blockly.Lua.ORDER_NONE];
};
